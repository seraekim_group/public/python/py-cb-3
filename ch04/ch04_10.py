"""
4.10 인덱스-값 페어 시퀀스 순환
문제: 시퀀시 순환 시 번호를 알고 싶다.
해결: enumerate()
"""

from collections import defaultdict
my_list = ['a', 'b', 'c']
for idx, val in enumerate(my_list):
    print(idx, val)

for idx, val in enumerate(my_list, 1):
    print(idx, val)

# 파일의 각 단어를 키로 가지고, 값은 라인의 리스트
word_summary = defaultdict(list)

with open('ch04_10.py', encoding='UTF-8') as f:
    lines = f.readlines()

for idx, line in enumerate(lines):
    words = [w.strip().lower() for w in line.split()]
    for word in words:
        word_summary[word].append(idx)

print(word_summary)

# enumerate 와 튜플
data = [(1, 2), (3, 4), (5, 6), (7, 8)]
for n, (x, y) in enumerate(data):
    print(n, x, y)

