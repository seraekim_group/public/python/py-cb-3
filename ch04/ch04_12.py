"""
4.12 서로 다른 콘테이너 아이템 순환
해결: itertools.chain(), 모든 아이템에 동일한 작업 수행
"""

from itertools import chain
a = [1, 2, 3, 4]
b = ['x', 'y', 'z']
for x in chain(a, b):
    print(x)

active_items = set()
inactive_items = set()

for item in chain(active_items, inactive_items):
    print()

# 비효율적
# for x in a + b:

# 효율적
# for x in chain(a, b):


