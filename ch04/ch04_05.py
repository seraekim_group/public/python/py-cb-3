"""
4.5 역방향 순환
해결: reversed(), 객체가 __reversed__() 특별 메소드를 구현하고 있거나 크기를 아는 경우만 가능
"""

a = [1, 2, 3, 4]
for x in reversed(a):
    print(x)

f = open('ch04_05.py', encoding='UTF-8')
for line in reversed(list(f)):
    print(line, end='')


# 역방향 이터레이터를 정의하면, 리스트로 변환하는 수고를 덜어준다.
class Countdown:
    def __init__(self, start):
        self.start = start

    # 순방향 순환
    def __iter__(self):
        n = self.start
        while n > 0:
            yield n
            n -= 1

    # 역방향 순환
    def __reversed__(self):
        n = 1
        while n <= self.start:
            yield n
            n += 1

