"""
4.3 제너레이터로 새로운 순환 패턴 생성
문제: 내장 함수(range, reversed) 와는 다른 동작을 하는 순환 패턴을 만들고 싶다.
해결: generator
"""


def frange(start, stop, increment):
    x = start
    while x < stop:
        yield x
        x += increment


for n in frange(0, 4, 0.5):
    print(n)

print(list(frange(0, 1, 0.125)))


# 일반함수완 다르게 제너레이터는 순환에 응답
def countdown(n):
    print('Starting to count from', n)
    while n > 0:
        yield n
        n -= 1
    print('Done! {}'.format(n))


# 제너레이터 함수는 순환에 의한 다음 연산에 응답하기 위해서만 실행된다
# 일반적으로 for문이 상세 내역을 책임지니 신경쓸거 없다.
c = countdown(3)
print(c)
for e in c:
    print(e)

# print(next(c))
# print(next(c))
# print(next(c))

