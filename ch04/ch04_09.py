"""
4.9 가능한 모든 순열과 조합 순환
해결: 순열 - itertools.permutations(), 조합 - itertools.combinations()
"""

from itertools import permutations, combinations, combinations_with_replacement
items = ['a', 'b', 'c']
for p in permutations(items):
    print(p)

for p in permutations(items, 2):
    print(p)

for c in combinations(items, 3):
    print(c)

for c in combinations(items, 2):
    print(c)

for c in combinations(items, 1):
    print(c)

# 조합에서 중복 선택 가능하게
for c in combinations_with_replacement(items, 3):
    print(c)
    