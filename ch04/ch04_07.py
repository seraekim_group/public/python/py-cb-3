"""
4.7 이터레이터의 일부 얻기
문제: 이터레이터가 만드는 데이터의 일부를 얻고 싶지만, 일반적인 자르기 연산자가 동작하지 않는다.
해결: itertools.islice()
"""

import itertools


def count(n):
    while True:
        yield n
        n += 1


c = count(0)

for x in itertools.islice(c, 10, 20):
    print(x)

# 만약에 뒤로 돌아가는 동작이 중요하다면 list 변환을 해라
