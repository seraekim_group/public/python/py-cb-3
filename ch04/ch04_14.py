"""
4.14 중첩 시퀀스 풀기
해결: yield from
"""

from collections.abc import Iterable


def flatten(items, ignore_types=(str, bytes)):
    for x in items:
        if isinstance(x, Iterable) and not isinstance(x, ignore_types):  # 문자열을 문자로 쪼개는 것 방지
            yield from flatten(x)
            # for i in flatten(x):  # yield from 대신에 쓸 수 있음.
            #     yield i
        else:
            yield x


items = [1, 2, [3, 4, [5, 6], 7], 8]

for x in flatten(items):
    print(x)

items2 = ['srkim', 'pati', ['laura', 'oliwia']]
for x in flatten(items2):
    print(x)
