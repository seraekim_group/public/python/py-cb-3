"""
4.1 수동으로 이터레이터 소비
문제: 순환 가능한 아이템에 접근 시 for 를 사용하기 싫다.
해결: next(), StopIteration 정의
"""

with open('ch04_01.py', encoding='UTF-8') as f:
    try:
        while True:
            line = next(f)
            # print(line, end='')
    except StopIteration:
        pass

# next 사용시, 종료값 지정 가능
with open('ch04_01.py', encoding='UTF-8') as f:
    while True:
        line = next(f, None)
        if line is None:
            break
        print(line, end='')

items = [1, 2, 3]
it = iter(items)  # items.__iter__()
print(next(it))  # it.__next__()
print(next(it))
print(next(it))


