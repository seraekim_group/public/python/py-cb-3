"""
4.16 무한 while 순환문을 이터레이터로 치환
해결: iter()
"""

CHUNKSIZE = 8192


def reader(s):
    while True:
        data = s.recv(CHUNKSIZE)
        if data == b'':
            break
        # process_data(data)


def reader2(s):
    for chunk in iter(lambda: s.recv(CHUNKSIZE), b''):
        pass
        # process_data(data)


# iter를 쓰면 EOF 확인 없이 한번에 가능
import sys
f = open('ch04_16.py', encoding='UTF-8')
for chunk in iter(lambda: f.read(10), ''):
    n = sys.stdout.write(chunk)
    print(n)

