"""
4.4 이터레이터 프로토콜 구현
해결: generator
"""


class Node:
    def __init__(self, v):
        self._v = v
        self._c = []

    def __repr__(self):
        return 'Node({!r})'.format(self._v)

    def add_c(self, node):
        self._c.append(node)

    def __iter__(self):
        return iter(self._c)

    def depth_first(self):
        yield self
        for c in self:
            yield from c.depth_first()  # 자식 순환


if __name__ == '__main__':
    root = Node(0)
    c1 = Node(1)
    c2 = Node(2)
    root.add_c(c1)
    root.add_c(c2)
    c1.add_c(Node(3))
    c1.add_c(Node(4))
    c2.add_c(Node(5))

    for ch in root.depth_first():
        print(ch)

