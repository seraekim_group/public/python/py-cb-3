"""
4.6 추가 상태를 가진 제너레이터 함수 정의
해결: __iter__() 에 제너레이터 사용하여 클래스 구현
"""

from collections import deque


class linehistory:
    def __init__(self, lines, histlen=3):
        self.lines = lines
        self.history = deque(maxlen=histlen)

    def __iter__(self):
        for lineno, line in enumerate(self.lines, 1):
            yield 'def'
            # yield line
            self.history.append((lineno, line))

    def clear(self):
        self.history.clear()


# 인스턴스를 만들기에 history 속성이나 clear() 메소드 등에 접근 가능
cnt = 1
with open('ch04_06.py', encoding='UTF-8') as f:
    lines = linehistory(f)
    # print(list(lines))
    for line in lines:
        if 'def' in line:
            for lineno, hline in lines.history:
                print('{} - {}:{}'.format(cnt, lineno, hline), end='')
                cnt += 1

# for문을 쓰지 않는 경우.. iter를 먼저 호출하고, 순환을 시작한다.
f = open('ch04_06.py', encoding='UTF-8')
lines = linehistory(f)
it = iter(lines)
print(next(it))
print(next(it))

