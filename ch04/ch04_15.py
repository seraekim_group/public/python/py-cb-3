"""
4.15 정렬된 여러 시퀀스를 병합 후 순환
해결: heapq.merge()
"""

import heapq
a = [1, 4, 7, 10]
b = [2, 5, 6, 11, -1, 5]
# b = ['x', 'y', 'z']
for c in heapq.merge(a, b):
    print(c)

