"""
4.13 데이터 처리 파이프라인 생성
해결: 제너레이터, 순환 처리로 인해 소비되는 메모리는 아주 적다
"""

import os, fnmatch, re, gzip, bz2


def gen_find(filepat, top):
    for path, dirlist, filelist in os.walk(top):
        for name in fnmatch.filter(filelist, filepat):
            yield os.path.join(path, name)


def gen_opener(filenames):
    for filename in filenames:
        if filename.endswith('.gz'):
            f = gzip.open(filename, 'rt')
        elif filename.endswith('.bz2'):
            f = bz2.open(filename, 'rt')
        else:
            f = open(filename, 'rt')
        yield f
        f.close()


def gen_concatenate(iterators):
    for it in iterators:
        yield from it


def gen_grep(pattern, lines):
    pat = re.compile(pattern)
    for line in lines:
        if pat.search(line):
            yield line


lognames = gen_find('localhost_access_log*', './')
files = gen_opener(lognames)
lines = gen_concatenate(files)
css_lines = gen_grep('css', lines)
for l in css_lines:
    print(l)
bytecolumn = (line.rsplit(None, 1)[1] for line in css_lines)
bytes = (int(x) for x in bytecolumn if x != '-')
print('Total', sum(bytes))

# David Beazley, Generator Tricks for Systems Programmers 참고
