"""
4.2 델리게이팅 순환
문제: 순환 가능한 객체를 담은 컨테이너에 사용가능한 iterator를 만들고 싶다.
해결: __iter__()
"""


class Node:
    def __init__(self, v):
        self._v = v
        self._c = []

    def __repr__(self):
        return 'Node({!r})'.format(self._v)

    def add_child(self, node):
        self._c.append(node)

    # 순환요청을 _c 속성으로 전달
    def __iter__(self):
        return iter(self._c)  # _c.__iter__() 호출


if __name__ == '__main__':
    root = Node(0)
    c1 = Node(1)
    c2 = Node(2)
    root.add_child(c1)
    root.add_child(c2)
    for ch in root:
        print(ch)
    # Node(1), Node(2) 출력

