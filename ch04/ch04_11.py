"""
4.11 여러 시퀀스 동시에 순환
해결: zip()
"""

xpts = [1, 5, 4, 2, 10, 7]
ypts = [101, 78, 37, 15, 62, 99]
for x, y in zip(xpts, ypts):
    print(x, y)

# 순환의 길이는 입력된 시퀀스 중 짧은 것
a = [1, 2, 3]
b = ['w', 'x', 'y', 'z']
for i in zip(a, b):
    print(i)

from itertools import zip_longest
for i in zip_longest(a, b):
    print(i)

for i in zip_longest(a, b, fillvalue=0):
    print(i)

c = [10, 11, 12]

for i in zip(a, b, c):
    print(i)

print(list(zip(a, b, c)))
