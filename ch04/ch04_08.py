"""
"
"
4.8 순환 객체 첫 번째 부분 건너뛰기
해결: itertools.dropwhile()
"""


from itertools import dropwhile, islice
with open('ch04_08.py', encoding='UTF-8') as f:
    for line in dropwhile(lambda line: line.startswith('"'), f):
        print(line, end='')

items = ['a', 'b', 'c', 1, 4, 10, 15]
for x in islice(items, 3, None):  # None = [3:]
    print(x)

# 전체에 걸쳐서 필터링
with open('ch04_08.py', encoding='UTF-8') as f:
    lines = (line for line in f if not line.startswith('"'))
    for line in lines:
        print(line, end='')
