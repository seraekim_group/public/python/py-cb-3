"""
10.5 공통된 네임스페이스에 코드 임포트 디렉터리 만들기
문제: 별도의 이름 있는 패키지로 설치하지 않고 모든 부분을 공통된 패키지 접두어로 합치고 싶다.
해결: sys.path.extend / __init__.py 생략, 이를 namespace package 라 한다.
"""

import sys
sys.path.extend(['foo', 'bar'])  # ch10/ch10_05/foo

import spam  # 에디터 상에서는 빨간줄이 뜨지만 실행 시 정상 작동..
print(spam.__path__)  # _NamespacePath(['foo\\spam', 'bar\\spam'])
import spam.blah
import spam.grok

# __file__ 속성을 확인했을 때 찾을 수 없다면 이 패키지는 namespace 이다.
print(spam.__file__)  # None
