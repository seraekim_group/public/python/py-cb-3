"""
10.2 일괄 임포트 제어
해결: _로 시작 또는 __all__
"""


def spam():
    pass


def grok():
    pass


blah = 42

__all__ = ['spam', 'grok']
