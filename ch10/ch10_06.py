"""
10.6 모듈 리로드
문제: 소스 코드에 수정을 했기 때문에 이미 불러온 모듈을 다시 불러오고 싶다.
해결: imp.reload()
"""

import ch10.ch10_05.ch10_05.foo.spam as spam
import importlib
importlib.reload(spam)

# 운영환경이라면 매우 위험.
# from x import x 같은 형식에서는 갱신하지 않는다.
# import x
# x.x() 같은 경우가 갱신 가능


