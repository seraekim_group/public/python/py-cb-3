"""
10.3 상대 경기 패키지 서브모듈 임포트
"""

# 같은 상대경로
from . import ch10_01

# 다른 상대경로
from ..ch09 import ch09_02

# 스크립트 실행 시..
# python -m ch10.ch10_02.spam
