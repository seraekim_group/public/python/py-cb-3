"""
10.9 sys.path에 디렉터리 추가
해결: PYTHONPATH 환경변수 추가 또는 .pth 파일 생성
"""

# env PYTHONPATH=/a/b python

# .pth 파일은 site-packages 디렉터리에 넣어야 함.

# xxx.pth
# /a/b
# /c/d
# ...

# 이 파일의 경로들이 sys.path에 추가 됨.

# import sys
# sys.path.insert(0, '/a/b') 이 방식도 동작하지만 매우 위험

# 실행코드가 위치한 디렉터리에 src 디렉터리를 추가하기
import sys
from os.path import abspath, join, dirname
sys.path.insert(0, abspath(dirname(__file__) + '/ch10_08'))
print(sys.path)
