"""
10.14 새로운 파이썬 환경 생성
해결: pyvenv 명령어로 가상환경을 만든다.
"""

# $ pyvenv Spam
# $ Spam/bin/python
# >>> from pprint import pprint
# >>> import sys
# >>> pprint(sys.path)

# 가상환경에서 이미 설치된 패키지를 포함시키고 싶다면..
# $ pyvenv --system-site-packages Spam

