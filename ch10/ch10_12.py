"""
10.12 임포트 시 모듈 패치
해결: import hook, decorator
"""

from functools import wraps
from ch10.ch10_12.postimport import when_imported
@when_imported('threading')
def warn_threads(mod):
    print('Threads? Are you crazy?')

import threading


# 더 실용적인 예제
def logged(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        print('Calling', func.__name__, args, kwargs)
        return func(*args, **kwargs)
    return wrapper

# 에제
@when_imported('math')
def add_logging(mod):
    mod.cos = logged(mod.cos)
    mod.sin = logged(mod.sin)

import math

math.sin(20)
math.cos(20)
