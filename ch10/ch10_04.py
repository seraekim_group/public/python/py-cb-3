"""
10.4 모듈을 여러 파일로 나누기
"""

import ch10.ch10_04 as c10
import ch10.ch10_04.a
a1 = c10.a.A
a2 = c10.A()
a2.spam()


# 단점
# 상속, 타입 확인 등에서 문제 생김.. 다음과 같이 써야 함.

if isinstance(a2, c10.a.A):
    print('hel')

