"""
10.13 개인적인 목적으로  패키지 설치
해결: --user 옵션
"""

# pip install --user package_name
# ~/.local/lib/python/site-packages
# 사용자 전용 디렉터리 설치이므로 sudo 권한이 필요하지 않음.

