"""
10.10 문자열로 주어진 모듈 이름 임포트
해결: importlib.import_module()
"""

import importlib
math = importlib.import_module('math')
print(math.sin(2))

# 상대경로로 임포트도 가능
# from . import b
b = importlib.import_module('ch10.ch10_04.a')
print(b)
