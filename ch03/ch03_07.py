"""
3.7 무한대와 NaN 사용
해결: float
"""

import math

a = float('inf')
b = float('-inf')
c = float('nan')

print(math.isnan(c))

print(10 / a)
print(a / a)
print(a + b)
print(math.sqrt(c))

# nan은 비교 불가. math.isnan 만 가능
print(c == float('nan'))
print(c is float('nan'))

# nan 이나 inf 값에 예외를 던지고 싶겠지만 그렇지 않은 이유는 플랫폼에 따라 동작이 다르기 때문
# fpectl 모듈을 활용하면 되지만, 숙련가들을 위한 것
