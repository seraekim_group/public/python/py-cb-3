"""
3.8 분수 계산
해결: fractions
"""
from fractions import Fraction
a = Fraction(5, 4)
b = Fraction(7, 16)
print(a + b)
c = a * b
print(c)
print(c.numerator)
print(c.denominator)

# 소수 변환
print(float(c))

# 분모 제한 근사값
print(c.limit_denominator(16))

# 소수를 분수로 변환
x = 3.75
print(*x.as_integer_ratio())
print(Fraction(*x.as_integer_ratio()))
