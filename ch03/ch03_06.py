"""
3.6 복소수 계산
해결: complex, j
"""

a = complex(2, 4)
b = 3 - 5j
print(a)
print(b)

print(a.real)
print(a.imag)

print(a + b)
print(a * b)
print(a / b)
print(abs(a))

# 사인 코사인 등
import cmath
print(cmath.sin(a))
print(cmath.cos(a))
print(cmath.exp(a))

import numpy as np
a = np.array([2+3j, 4+5j, 6-7j, 8+9j])
print(a)
print(a + 2)
print(np.sin(a))

print(cmath.sqrt(-1))
