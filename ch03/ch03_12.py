"""
3.12 시간 단위 변환
해결: datetime
"""

from datetime import timedelta  # 시간의 간격
from datetime import datetime  # 특정 시간
a = timedelta(days=2, hours=6)
b = timedelta(hours=4.5)
c = a + b
print(c.days)
print(c.seconds)
print(c.seconds / 3600)
print(c.total_seconds() / 3600)

a = datetime(2012, 9, 23)
print(a + timedelta(days=10))
b = datetime(2012, 12, 21)
d = b - a
print(d.days)

now = datetime.today()
print(now)
print(now + timedelta(minutes=10))

# 윤년 인식
a = datetime(2012, 3, 1)
b = datetime(2012, 2, 28)
print(a - b)
print((a - b).days)

a = datetime(2013, 3, 1)
b = datetime(2013, 2, 28)
print((a - b).days)

# timezone, fuzzy time rage, 공휴일 등 더 복잡한건 dateutil
# python-dateutil
from dateutil.relativedelta import relativedelta
print(a + relativedelta(months=+1))
print(a + relativedelta(months=+4))

# 두 날짜 사이의 시간
a = datetime(2012, 9, 23)
b = datetime(2012, 12, 21)
print(b - a)
d = relativedelta(b, a)
print(d)
print(d.months)
print(d.days)
