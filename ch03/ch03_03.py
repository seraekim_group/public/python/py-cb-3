"""
3.3 출력을 위한 숫자 서식화
문제: 출력을 위해 자릿수, 정렬, 천 단위 구분 등 숫자를 서식화하고 싶다.
해결: format
"""

x = 1234.56789
print(format(x, '0.2f'))
print(format(x, '<10.1f'))
print(format(x, '>10.1f'))
print(format(x, '^10.1f'))
print(format(x, ','))
print(format(x, '0,.1f'))

# 지수 표현법
print(format(x, 'e'))
print(format(x, '0.2E'))

print(format(1.2545, '0.3f'))
print(format(-1.2545, '0.3f'))

# 구분자 문자 변경
swap_separators = {ord('.'):',', ord(','):'.'}
print(format(x, ',').translate(swap_separators))

# 기능은 format 보다 적지만 간단한 % 연산자
print('%0.2f' % x)
print('%10.1f' % x)
print('%-10.1f' % x)
