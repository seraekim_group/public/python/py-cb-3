"""
3.13 마지막 금요일 날짜 구하기
해결: datetime
"""

from datetime import datetime, timedelta
weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']


def get_previes_byday(dayname, start_date=None):
    if start_date is None:
        start_date = datetime.today()
    day_num = start_date.weekday()
    day_num_target = weekdays.index(dayname)
    days_ago = (7 + day_num - day_num_target) % 7
    if days_ago == 0:
        days_ago = 7
    target_date = start_date - timedelta(days=days_ago)
    return target_date


print(datetime.today())
print(get_previes_byday('Monday'))
print(get_previes_byday('Tuesday'))
print(get_previes_byday('Friday'))

print(get_previes_byday('Friday', datetime(2012, 12, 21)))

# dateutil 을 사용한다면..
from dateutil.relativedelta import relativedelta
from dateutil.rrule import *
d = datetime.now()
print(d)
print(d + relativedelta(weekday=FR))
print(d + relativedelta(weekday=FR(-1)))
