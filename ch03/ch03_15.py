"""
3.15 문자열을 시간으로 변환
문제: 문자열 형식의 시간 데이터를 datetime 객체로 변환
해결: datetime.strptime
"""

from datetime import datetime
text = '2012-09-20'
y = datetime.strptime(text, '%Y-%m-%d')
z = datetime.now()
diff = z - y
print(diff)

nice_z = datetime.strftime(z, '%A %B %d, %Y')
print(nice_z)


# strptime은 예상보다 실행속도가 느린 경우가 많다.
# 직접 구현 하자면...
def parse_ymd(s):
    year_s, mon_s, day_s = s.split('-')
    return datetime(int(year_s), int(mon_s), int(day_s))

