"""
3.10 행렬과 선형 대수 계산
해결: Numpy.matrix
"""

import numpy as np
import numpy.linalg as lin

m = np.matrix([[1, -2, 3], [0, 4, 5], [7, 8, -9]])
# transpose
print(m.T)
# inverse
print(m.I)
v = np.matrix([[2], [3], [4]])
print(m * v)

# determinant
print(lin.det(m))

# eigenvalues
print(lin.eigvals(m))

# mx = v, x?
x = lin.solve(m, v)
print(x)
print(m * x)
print(v)




