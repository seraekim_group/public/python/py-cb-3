"""
3.1 반올림
문제: 부동 소수점 값을 10진수로 반올림 하고 싶다.
해결: round
"""
print(round(-1.27, 1))
print(round(1.2545, 3))  # 1.255가 아니다.
print(round(1.25451, 3))  # 1.255

# 값이 정확히 두 선택지의 가운데 있으면 더 가까운 짝수가 된다.
print(round(1.5, 0))
print(round(2.5, 0))

a = 1627731
print(round(a, -1))
print(round(a, -3))

# 특정 자리수 까지만 표시하는 거면, round가 아닌 format 사용
x = 1.23456
print('value is {:0.3f}'.format(x))

# 부동소수점 문제 해결하려고 round 쓰는 것은 지양, decimal 쓰라
a = 2.1
b = 4.2
c = a + b
print(c)
print(round(c, 2))
