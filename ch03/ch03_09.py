"""
3.9 큰 배열 계산
해결: numpy
"""

import numpy as np
x = [1, 2, 3, 4]
y = [5, 6, 7, 8]
print(x * 2)
# print(x + 10)
print(x + y)

# 스칼라 연산이 요소 기반으로 적용
ax = np.array([1, 2, 3, 4])
ay = np.array([5, 6, 7, 8])
print(ax * 2)
print(ax + 10)
print(ax + ay)
print(ax * ay)


# 다항식 계산
def f(x):
    return 3*x**2 - 2*x + 7


print(f(ax))

# 일반함수 제공, 배열요소 순환하는 math 모듈보다 수백 배 빠르다
print(np.sqrt(ax))
print(np.cos(ax))

# 10000 * 10000 2차원 그리드
grid = np.zeros(shape=(10000, 10000), dtype=float)
print(grid)

grid += 10
print(grid)
print(np.sin(grid))

# 다차원 배열
a = np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]])
print(a)
print(a[1])
# 첫번째 열 선택
print(a[:, 1])
print(a[1:3, 1:3])
a[1:3, 1:3] += 10
print(a)
# 행 벡터
print(a + [100, 101, 102, 103])
# 조건 할당
print(np.where(a > 10, a, 10))
print(np.where(a > 10, 10, a))



