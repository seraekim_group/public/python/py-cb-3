"""
3.5 바이트에서 큰 숫자를 패킹/언패킹
문제: byte string unpacking 해서 정수 값 생성, 혹은 정수 값을 바이트 문자열로
해결: from_bytes, to_bytes
"""

data = b'\x00\x124v\x00x\x90\xab\x00\xcd\xef\x01\x00#\x004'
print(len(data))
print(int.from_bytes(data, 'little'))
print(int.from_bytes(data, 'big'))

x = 94525377821947740945920721189797940
print(x.to_bytes(16, 'big'))
print(x.to_bytes(16, 'little'))

# 정수형 값과 바이트 문자열 변환은 일반적인 작업은 아니다. 네트워크, 암호화 등에서 사용한다.
# IPv6 주소를 128비트 정수형으로 표시하는 경우

x = 523 ** 23

nbytes, rem = divmod(x.bit_length(), 8)
if rem:
    nbytes += 1

print(nbytes)
print(rem)
print(x.to_bytes(nbytes, 'little'))

