"""
3.16 시간대 관련 날짜 처리
해결: pytz
"""

# pytz 는 Olson 시간대 데이터베이스를 제공

from datetime import datetime, timedelta

import pytz
from pytz import timezone

d = datetime(2012, 12, 21, 9, 30, 0)
print(d)

# 시카고에 맞게 현지화
central = timezone('US/Central')
loc_d = central.localize(d)
print(loc_d)

# 방갈로르
bang_d = loc_d.astimezone(timezone('Asia/Kolkata'))

# 서머타임제(daylight saving time)
# 2013년 미국에선 3월 13일 오전2시에 시작
d = datetime(2013, 3, 10, 1, 45)
loc_d = central.localize(d)
print(loc_d)
later = loc_d + timedelta(minutes=30)
later2 = central.normalize(loc_d + timedelta(minutes=30))
print(later)
print(later2)


# UTC 변환, 서머타임 같은 자잘한 문제를 신경쓸 필요 없음.
utc_d = loc_d.astimezone(pytz.utc)
print(utc_d)

later_utc = utc_d + timedelta(minutes=30)
print(later_utc)

# 국가별 키값 알아내기
print(pytz.country_timezones['IN'])
print(pytz.country_timezones['KR'])
# for n in pytz.country_timezones:
#     print(n)

