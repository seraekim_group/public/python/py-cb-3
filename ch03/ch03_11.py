"""
3.11 임의의 요소 뽑기
해결: random
"""

import random
values = [1, 2, 3, 4, 5, 6]

# 임의의 아이템 선택
print(random.choice(values))
print(random.choice(values))
print(random.choice(values))
print(random.choice(values))
print(random.choice(values))

# 임의의 아이템 N개 뽑고 버리기
print(random.sample(values, 2))
print(random.sample(values, 2))
print(random.sample(values, 3))
print(random.sample(values, 3))

# 무작위로 섞기
random.shuffle(values)
print(values)

# 임의의 정수 생성
print(random.randint(0, 10))
print(random.randint(0, 10))
print(random.randint(0, 10))

# 0과 1 사이의 균등 부동 소수점
print(random.random())
print(random.random())
print(random.random())

# N비트 정수
print(random.getrandbits(200))

# random 모듈은 Mersenne Twister 알고리즘으로 난수 발생
# seed로 시드 값 바꿀 수 있다.

random.seed()
random.seed(12345)
random.seed(b'bytedata')