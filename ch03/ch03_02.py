"""
3.2 정확한 10진수 계산
문제: 부동 소수점 오류를 피하고 싶다.
해결: 성능 측면을 희생할 용의가 있다면 decimal
"""

# CPU와 IEEE 754로 부동 소수점 숫자 계산을 할 때 필연적으로 발생

from decimal import Decimal
a = Decimal('4.2')
b = Decimal('2.1')
print(a + b)
print(Decimal(6.3))
print(a + b == Decimal('6.3'))  # True
print(a + b == 6.3)  # False
print(4.2 + 2.1 == 6.3)  # False

# 반올림 자릿 수 조정
a = Decimal('1.3')
b = Decimal('1.7')
print(a / b)

from decimal import localcontext

with localcontext() as ctx:
    ctx.prec = 3
    print(a / b)

with localcontext() as ctx:
    ctx.prec = 50
    print(a / b)

# decimal은 IBM General Decimal Arithmetic Specification 을 구현

# 의외로 과학, 공학, 컴퓨터 글픽 등 자연 과학 영역을 다룰 때는 부동 소수점 값을 사용하는 것이 일반적
# float 17자리 정확도로 거의 표현 가능, float의 실행속도가 확연히 빠름

nums = [1.23e+18, 1, -1.23e+18]
print(sum(nums))  # 1이 사라짐

import math
print(math.fsum(nums))

# 금융 데이터를 다룰 땐 decimal이 필수
