"""
3.4 2진수, 8진수, 16진수 작업
문제: 2, 8, 16진수 출력
해결: bin, oct, hex, format
"""

x = 1234
print(bin(x))
print(oct(x))
print(hex(x))

# 0b 0o 0x 붙는게 싫다면 format
print(format(x, 'b'))
print(format(x, 'o'))
print(format(x, 'x'))

# 부호가 없는 값을 보여주려면 비트길이 설정
print(format(-x, 'b'))
print(format(2**32 + x, 'b'))
print(format(2**32 + -x, 'b'))
print(format(2**32 + x, 'x'))
print(format(2**32 + -x, 'x'))

# 다른 진법의 숫자 변환은 int
print(int('4d2', 16))
print(int('10011010010', 2))

# 파이썬의 8진법은 0o를 붙여줘야 함
0o755  # 0755
