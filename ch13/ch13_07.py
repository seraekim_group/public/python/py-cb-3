"""
13.7 파일과 디렉터리 복사와 이동
"""

import shutil

# cp src dst
shutil.copy('src', 'dst')

# 메타데이터보존, cp -p src dst
shutil.copy('src', 'dst')

# 트리 복사 cp -R src dst
shutil.copytree('src', 'dst')

# 이동 mv src dst
shutil.move('src', 'dst')

# 심볼릭 링크 복사
shutil.copy2('src', 'dst', follow_symlinks=False)

# 링크보존
shutil.copytree('src', 'dst', symlinks=True)

# 특정 파일명 패턴 무시하기
shutil.copytree('src', 'dst', ignore=shutil.ignore_patterns('*~', '*.pyc'))

# os 간 호환성을 위해..
from os import path

filename = 'ch13_07.py'
print(path.basename(filename))
print(path.dirname(filename))
print(path.split(filename))
print(path.join('/new/dir', filename))
print(path.expanduser(filename))

# copytree 에러 처리...
try:
    shutil.copytree('src', 'dst', ignore_dangling_symlinks=True)  # 잘못된 심링크 무시
except shutil.Error as e:
    for src, dst, msg in e.args[0]:
        print(dst, src, msg)

