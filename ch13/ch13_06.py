"""
13.6 외부 명령을 실행하고 결과 얻기
"""

import subprocess
import shlex

try:
    # out_bytes = subprocess.check_output(['netstat', '-a'])

    # 표준 출력과 에러를 모두 원한다면 stderr 인자 필요
    out_bytes = subprocess.check_output(['netstat', '-a'], stderr=subprocess.STDOUT, timeout=5)

    # 복잡한 쉘 커맨드를 실행 시 shell 인자 추가
    # out_bytes = subprocess.check_output('grep python | wc > out', shell=True)
    # 쉘 커맨드 인자를 따옴표로 묶을 때 shlex.quote()
except subprocess.CalledProcessError as e:
    print(e)
    out_bytes = e.output
    code = e.returncode

print(out_bytes)
out_text = out_bytes.decode('euc-kr')
print(out_text)

# 서브프로세스와 입력 보내기 등 좀 더 복잡한 통신을 해야 한다면..
text = b'''
hell wolrd
To jest test
dobranoc
'''

# 파이프와 함께 명령 실행
# ['wc']
p = subprocess.Popen(['find', '/c', '/v', ''], stdout=subprocess.PIPE, stdin=subprocess.PIPE)

# 데이터를 전송하고 결과 얻기
stdout, stderr = p.communicate(text)

# 텍스트로 해석하기 위한 디코딩
print(stdout)
out = stdout.decode('utf-8')
print(out)
# err = stderr.decode('euc-kr')
# print(err)
