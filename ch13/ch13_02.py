"""
13.2 에러 메시지와 함께 프로그램 종료
"""

import sys

# 상태코드 1을 반환하며 종료
#raise SystemExit('It failed!')

sys.stderr.write('It failed!\n')
raise SystemExit(1)
