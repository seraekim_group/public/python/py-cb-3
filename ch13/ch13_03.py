"""
13.3 커맨드 라인 옵션 파싱
"""

import argparse

# store - 하나의 값 저장
# append - list 저장
# action - 인자 관련 처리 명시

parser = argparse.ArgumentParser(description='Search some files')
parser.add_argument(dest='filenames', metavar='filename', nargs='*')
parser.add_argument('-p', '--pat', metavar='pattern', required=True,
                    dest='patterns', action='append',
                    help='text pattern to search for')
parser.add_argument('-v', dest='verbose', action='store_true', help='verbose mode')
parser.add_argument('-o', dest='outfile', action='store', help='output file')
parser.add_argument('--speed', dest='speed', action='store',
                    choices=['slow', 'fast'], default='slow',
                    help='search speed')
args = parser.parse_args()

print(args.filenames)
print(args.patterns)
print(args.verbose)
print(args.outfile)
print(args.speed)

# python ch13_03.py -h
# python ch13_03.py -v -p import --pat=from ../ch12/ch12_11.py -o results
