"""
13.13 스톱워치 타이머 만들기
"""

import time

class Timer:
    def __init__(self, func=time.perf_counter):
        self.elapsed = 0.0
        self._func = func
        self._start = None

    def start(self):
        if self._start is not None:
            raise RuntimeError('Already started')
        self._start = self._func()

    def stop(self):
        if self._start is None:
            raise RuntimeError('Not started')
        end = self._func()
        self.elapsed += end - self._start
        self._start = None

    def reset(self):
        self.elapsed = 0.0

    @property
    def running(self):
        return self._start is not None

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop()


def countdown(n):
    while n > 0:
        n -= 1


t = Timer()
t.start()
countdown(10000000)
t.stop()
print(t.elapsed)

with t:
    countdown(10000000)

print(t.elapsed)

with Timer() as t2:
    countdown(10000000)

print(t2.elapsed)

# 처리할 때 걸린 CPU 시간만 필요하다면
t = Timer(time.process_time)
with t:
    countdown(10000000)
print(t.elapsed)

