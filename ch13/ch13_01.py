# !/usr/bin/env python3
"""
13.1 리다이렉션, 파이프, 입력 파일을 통한 스크립트 입력 받기
"""

import fileinput

with fileinput.input() as f:
    for line in f:
        print(f.filename(), f.lineno(), line, end='')

# py-cb-3\ch13>python ./ch13_01.py < ../ch12/ch12_01.py
