"""
13.12 라이브러리에 로그 추가
"""

import logging

# 전역 설정을 ERROR로 했으나, somelib 에서는 DEBUG로 했으므로 별도로 DEBUG 까지 출력 된다.
logging.basicConfig(level=logging.ERROR)
from ch13.ch13_12.somelib import func

func()

