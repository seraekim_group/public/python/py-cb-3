"""
13.11 간단한 스크립트에 로그 추가
"""

import logging
import logging.config


def main():
    # 로그 시스템 환경 설정
    # logging.basicConfig(level=logging.DEBUG)
    logging.config.fileConfig('logconfig.ini')

    hostname = 'www.python.org'
    item = 'spam'
    filename = 'data.csv'
    mode = 'r'

    logging.critical('Host %s unknown', hostname)
    logging.error("couldn't find %r", item)
    logging.warning('Feature is deprecated')
    logging.info('Opening file %r, mode=%r', filename, mode)
    logging.debug('Got here')


if __name__ == '__main__':
    main()

# basicConfig는 한번 쓰인 후에는 접근하여 수정을 못하므로
# logging.getLogger().level = ... 과 같이 수정해야 함.
