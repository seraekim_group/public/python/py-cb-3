"""
13.15 웹 브라우저 실행
"""

import webbrowser

webbrowser.register('chrome', None,
                    webbrowser.Chrome('C:/Program Files (x86)/Google/Chrome/Application/chrome.exe'))
# wb = webbrowser.Chrome('C:/Program Files (x86)/Google/Chrome/Application/chrome.exe')
wb = webbrowser.get('chrome')

wb.open('http://www.python.org')

# 새 창
wb.open_new('http://www.python.org')

# 새 탭
wb.open_new_tab('http://www.python.org')
