"""
5.1 텍스트 데이터 읽고 쓰기
문제: ASCII, UTF-8, UTF-16 같이 서로 다른 인코딩 사용
해결: open(), rt모드
"""

# 파일 전체를 하나의 문자열로 읽음
with open('ch05_01.py', 'rt', encoding='UTF-8') as f:
    data = f.read()

# 라인 순환
with open('ch05_01.py', 'rt', encoding='UTF-8') as f:
    for line in f:
        pass

# 파일을 쓰려면 wt, 이어쓰기는 at

# 줄바꿈 변환 없이 읽으려면 newline=''

# 에러 무시
with open('ch05_01.py', 'rt', errors='replace') as f:
    print(f.read())

with open('ch05_01.py', 'rt', errors='ignore') as f:
    print(f.read())
