"""
5.13 디렉터리 리스팅 구하기
해결: os.listdir()
"""

import os.path, glob
from fnmatch import fnmatch

names = [name for name in os.listdir('../')
         if os.path.isdir(os.path.join('../', name))]
print(names)

bin_files = [name for name in os.listdir('./')
             if name.endswith('.bin')]
print(bin_files)

txt_files = glob.glob('*.txt')
print(txt_files)
txt_files = [name for name in os.listdir('./')
             if fnmatch(name, '*.txt')]
print(txt_files)

# 파일 크기와 수정 날짜 구하기
name_sz_date = [(name, os.path.getsize(name), os.path.getmtime(name))
                for name in txt_files]

for name, size, mtime in name_sz_date:
    print(name, size, mtime)

file_metadata = [(name, os.stat(name)) for name in txt_files]
for name, meta in file_metadata:
    print(name, meta.st_size, meta.st_mtime)
