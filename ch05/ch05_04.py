"""
5.4 바이너리 데이터 읽고 쓰기
해결: rb, wb
"""

t = 'Hello World'
for c in t:
    print(c)

b = b'Hello World'
for c in b:
    print(c)

with open('ch05_04.bin', 'rb') as f:
    data = f.read(16)
    print(data)
    text = data.decode('utf-8')
    print(text)

with open('ch05_04.bin', 'wb') as f:
    text = 'hell world'
    f.write(text.encode('utf-8'))

import array
nums = array.array('i', [1, 2, 3, 4])
with open('ch05_04.bin', 'wb') as f:
    f.write(nums)

# buffer interface, c구조체 바로 쓰기

# readinto() 바이너리 데이터를 메모리로 읽어 들인다.
a = array.array('i', [0, 0, 0, 0, 0, 0, 0, 0])
with open('ch05_04.bin', 'rb') as f:
    f.readinto(a)
    print(a)
    
    