"""
5.11 경로 다루기
해결: os.path
"""

import os
path = '~/xxx.txt'
print(os.path.basename(path))
print(os.path.dirname(path))
print(os.path.join('tmp', 'data', os.path.basename(path)))
print(os.path.expanduser(path))
print(os.path.splitext(path))
