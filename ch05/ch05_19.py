"""
5.19 임시 파일과 디렉터리 만들기
해결: tempfile
"""

from tempfile import TemporaryFile, NamedTemporaryFile, TemporaryDirectory
import tempfile

# with TemporaryFile('w+t') as f:
with TemporaryFile('w+t', encoding='UTF-8') as f:
    f.write('Hello World\n')
    f.write('Testing\n')

    # 처음으로 이동
    f.seek(0)
    data = f.read()

# 임시파일은 파기

f = TemporaryFile('w+t')  # 바이너리 모드 w+b
f.close()

# 이름 및 디렉터리 엔트리 가지려면
with NamedTemporaryFile('w+t', delete=False) as f:
    print('filename is:', f.name)

with TemporaryDirectory() as dirname:
    print('dirname is:', dirname)

# 더 하위레벨로 내려가면...
print(tempfile.mkstemp())
print(tempfile.mkdtemp())
print(tempfile.gettempdir())

# 규칙 오버라이드
f = NamedTemporaryFile(prefix='srkim', suffix='.txt', dir='.')
print(f.name)

# tempfile은 접근권한은 현재 사용자 에게만, race condition 방지
