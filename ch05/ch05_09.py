"""
5.9 바이너리 데이터를 수정 가능한 버퍼에 넣기
해결: readinto()
"""

import os.path


def read_into_buffer(filename):
    buf = bytearray(os.path.getsize(filename))
    with open(filename, 'rb') as f:
        f.readinto(buf)
    return buf


buf = read_into_buffer('ch05_04.bin')
print(buf)

buf[0:5] = b'XXXXX'
print(buf)

with open('ch05_09.bin', 'wb') as f:
    f.write(buf)


# memoryview zero copy 조각, 기존 내용 수정하지 않음
buf = bytearray(b'Hell World')
m1 = memoryview(buf)
m2 = m1[-5:]
print(m2)

m2[:] = b'WORLD'
print(buf)
