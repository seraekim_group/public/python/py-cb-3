"""
5.21 파이썬 객체를 직렬화하기
해결: pickle
"""

import pickle

f_n = 'ch05_21.bin'

data = [1, 2, 3]
f = open(f_n, 'wb')
pickle.dump(data, f)
s = pickle.dumps(data)
print(s)

# 로드
f = open(f_n, 'rb')
data = pickle.load(f)
data2 = pickle.loads(s)
print(data2)

# 다중 객체와 작업
f = open(f_n, 'wb')
pickle.dump([1, 2, 3, 4], f)
pickle.dump('hello', f)
pickle.dump(['app', 'ban', 'grape'], f)
f.close()

f = open(f_n, 'rb')
print(pickle.load(f))
print(pickle.load(f))
print(pickle.load(f))

# getstate, setstae 활용
import time
# 쓰기
# from ch05.ch05_21_countdown import Countdown
# c = Countdown(30)
#
# time.sleep(3)
#
# f = open('ch05_21_countdown.bin', 'xb')
# pickle.dump(c, f)
# f.close()

#재시작
f = open('ch05_21_countdown.bin', 'rb')
pickle.load(f)
time.sleep(3)

# pickle은 array, mumpy 같은 거대한 자료에 사용하기에 효율적인 인코딩 방식이 아니다.