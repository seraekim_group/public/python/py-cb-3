"""
5.6 문자열에 입출력 작업하기
해결: io.StringIO(), io.BytesIO(), 일반 파일 기능 흉내 시 유용
"""

import io
s = io.StringIO()
s.write('hell world\n')
print('Tyhis is a test', file=s)

# 기록한 모든 데이터 얻기
print(s.getvalue())

# 기존 문자열을 파일 인터페이스로 감싸기
s = io.StringIO('Hello\nWorld\n')
print(s.read(4))
print(s.read())

b = io.BytesIO()
b.write(b'bin data')
print(b.getvalue())
