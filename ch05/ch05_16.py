"""
5.16 이미 열려 있는 파일의 인코딩을 수정하거나 추가하기
해결: io.TextIOWrapper()
"""

import urllib.request
import io
import sys

u = urllib.request.urlopen('http://www.python.org')
f = io.TextIOWrapper(u, encoding='utf-8')
text = f.read()

# detach로 인코딩 레이어 제거 후 치환
print(sys.stdout.encoding)
sys.stdout = io.TextIOWrapper(sys.stdout.detach(), encoding='latin-1')
print(sys.stdout.encoding)

# I/O 레이어
f = open('ch05_16.py')
print(f)  # io.TextIOWrapper 인코딩/디코딩 처리 레이어
print(f.buffer)  # io.BufferedReader binary 데이터 처리
print(f.buffer.raw)  # io.FileIO, OS 하위 레벨 file descriptor

# 라인처리, 에러 규칙 등에 활용
sys.stdout = io.TextIOWrapper(sys.stdout.detach(), encoding='ascii', errors='xmlcharrefreplace')
print('jalape\xc3\xb1o')
