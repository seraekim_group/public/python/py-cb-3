"""
5.3 구별자나 종단 부호 바꾸기
해결: print() sep, end
"""

print('ACME', 50, 91.5)
print('ACME', 50, 91.5, sep='.')
print('ACME', 50, 91.5, sep='.', end='!!\n')

for i in range(5):
    print(i, end=' ')

print()
row = ('ACME', '50', '91.5')

# 문자열에만 도착
print(','.join(row))
row = ('ACME', 50, '91.5')
print(','.join(str(x) for x in row))

print(*row, sep=',')
