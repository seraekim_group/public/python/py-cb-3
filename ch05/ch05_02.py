"""
5.2 파일에 출력
해결: print()에 file 키워드 인자 사용
"""

with open('ch05_02.txt', 'at') as f:
    print('Hell world', file=f)

