"""
5.8 고정 크기 레코드 순환
문제: 크기 지정해서 단위별로 순환
해결: iter(), functools.partial()
"""

from functools import partial

RECORD_SIZE = 32

with open('xxx.data', 'rb') as f:
    # 종료값 b'' 반환
    # 텍스트 경우 라인별 출력인데, 바이너리 경우 partial 고정값이 일반적
    records = iter(partial(f.read, RECORD_SIZE), b'')
    for r in records:
        print(r)

