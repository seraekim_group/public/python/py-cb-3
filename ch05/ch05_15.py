"""
5.15 망가진 파일 이름 출력
문제: 파일 이름 출력 시, UnicodeEncodeError / surrogates not allowed 에러 발생
해결: 다음 코드로 에러 방지
"""

import os
import sys


def bad_filename(filename):
    print('나쁜 이름')
    return repr(filename)[1:-1]


# 재인코딩
def bad_filename2(filename):
    temp = filename.encode(sys.getfilesystemencoding(), errors='surrogateescape')
    return temp.decode('latin-1')


filenames = os.listdir('.')

for filename in filenames:
    try:
        print(filename)
    except UnicodeEncodeError:
        # \udce4 는 첫 반쪽이 없는 surrogate pair 다
        print(bad_filename(filename))
        print(bad_filename2(filename))