"""
5.5 존재하지 않는 파일에 쓰기
해결: x 모드
"""

with open('ch05_05.txt', 'xt') as f:
    f.write('hell world\n')

# 파일을 쓰기전 확인
import os
if not os.path.exists('ch05_05.txt'):
    with open('ch05_05.txt', 'wt') as f:
        f.write('hell w')
else:
    print('File already exists!')

