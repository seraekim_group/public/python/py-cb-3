"""
5.17 텍스트 파일에 바이트 쓰기
해결: buffer
"""

import sys
# sys.stdout.write(b'Hello\n')
sys.stdout.buffer.write(b'Hello\n')

# buffer 속성에 접근 하면 인코딩/디코딩 레이어를 우회하기에 가능