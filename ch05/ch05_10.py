"""
5.10 바이너리 파일 메모리 매핑
해결: mmap
"""
import os
import mmap


def memory_map(filename, access=mmap.ACCESS_WRITE):
    size = os.path.getsize(filename)
    fd = os.open(filename, os.O_RDWR)
    return mmap.mmap(fd, size, access=access)


file_name = 'ch05_10.bin'
size = 1000000
with open(file_name, 'wb') as f:
    f.seek(size-1)
    f.write(b'\x00')

m = memory_map(file_name)
print(len(m))
print(m[0:10])
print(m[0])
m[0:10] = b'Hell World'
m.close()

with open(file_name, 'rb') as f:
    print(f.read(11))

# mmap as context manager
with memory_map(file_name) as m:
    print(len(m))
    print(m[0:10])

# 자동으로 닫혔는지 확인
print(m.closed)

# memory_map() 데이터에 수정을 하면 모두 원본 파일에 복사. 읽기전용 원하면 ACCESS_READ를 전달
# 지역 레벨에서 수정하고 원본 영향 안주려면 mmap.ACCESS_COPY

m = memory_map(file_name)
v = memoryview(m).cast('I')
v[0] = 7
print(m[0:4])
print(v[0])
