"""
5.7 압축된 데이터 파일 읽거나 쓰기
해결: gzip, bz2
"""
import gzip
import bz2

with gzip.open('xxx.gz', 'wt', compresslevel=9) as f:
    pass

# 기존에 열려 있는 바이너리 파일의 상위에 위치
f = open('xxx.gz', 'rb')
with gzip.open(f, 'rt') as g:
    text = g.read()

