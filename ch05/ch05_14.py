"""
5.14 파일 이름 인코딩 우회
해결: raw byte string
"""

import sys, os

print(sys.getfilesystemencoding())

# 디렉터리 리스트 (디코딩 됨)
print(os.listdir('.'))

# 디렉터리 리스트 (디코딩 안됨)
print(os.listdir(b'.'))

# 로우 파일 이름으로 파일 열기
with open(b'ch05_14_jalape\xc3\xb1o.txt') as f:
    print(f.read())

