"""
5.12 파일 존재 여부 확인
해결: os.path
"""

import os, time
f_name = './ch05_12.py'
print(os.path.exists(f_name))
print(os.path.exists('./ch05_12_2.py'))
print(os.path.isfile(f_name))
print(os.path.isdir(f_name))
print(os.path.islink(f_name))
print(os.path.realpath(f_name))
print(os.path.getsize(f_name))
print(time.ctime(os.path.getmtime(f_name)))

# 접근 권한 주의
