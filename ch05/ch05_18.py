"""
5.18 기존 파일 디스크립터를 파일 객체로 감싸기
해결:
"""

import os
import sys

# open file descriptor
fd = os.open('ch05_18.txt', os.O_WRONLY | os.O_CREAT)


f = open(fd, 'wt')
f.write('hell world\n')
f.close()

# 사용이 끝났을 때 fd를 닫지 않으려면, closedfd=False
# f = open(fd, 'wt', closefd=False)

bstdout = open(sys.stdout.fileno(), 'wb', closefd=False)
bstdout.write(b'Hallo Wolrd\n')
bstdout.flush()


