"""
7.3 함수 인자에 메타데이터 넣기
해결: function argument annotation
"""


def add(x: int, y: int) -> int:
    return x + y


help(add)
print(add.__annotations__)

