"""
7.10 콜백 함수에 추가적 상태 넣기
해결: bound-method, closure, coroutine, partial, lambda
"""


def apply_async(func, args, *, callback):
    result = func(*args)
    callback(result)


def print_result(result):
    print('Got:', result)


def add(x, y):
    return x + y


apply_async(add, (2, 3), callback=print_result)
apply_async(add, ('hello', 'world'), callback=print_result)


# bound method handler callback
class ResultHandler:
    def __init__(self):
        self.sequence = 0

    def handler(self, result):
        self.sequence += 1
        print('[{}] Got: {}'.format(self.sequence, result))


r = ResultHandler()
apply_async(add, (2, 3), callback=r.handler)
apply_async(add, ('hello', 'world'), callback=r.handler)


# closure
def make_handler():
    sequence = 0

    def handler(result):
        # 콜백내부에서 수정됨
        nonlocal sequence
        sequence += 1
        print('[{}] Got: {}'.format(sequence, result))
    return handler


handler = make_handler()
apply_async(add, (2, 3), callback=handler)
apply_async(add, ('hello', 'world'), callback=handler)


# coroutine
def make_handler2():
    sequence = 0
    while True:
        result = yield
        sequence += 1
        print('[{}] Got: {}'.format(sequence, result))


handler2 = make_handler2()
next(handler2)
apply_async(add, (2, 3), callback=handler2.send)
apply_async(add, ('hello', 'world'), callback=handler2.send)


# partial
class SequenceNo:
    sequence = 0
    # def __init__(self):
    #     self.sequence = 0


def handler3(result, seq):
    seq.sequence += 1
    print('[{}] Got: {}'.format(seq.sequence, result))


seq = SequenceNo()
from functools import partial
apply_async(add, (2, 3), callback=partial(handler3, seq=seq))
apply_async(add, ('hello', 'world'), callback=partial(handler3, seq=seq))

# lambda
apply_async(add, (2, 3), callback=lambda r: handler3(r, seq))
apply_async(add, ('hello', 'world'), callback=lambda r: handler3(r, seq))
