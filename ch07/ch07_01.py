"""
7.1 매개변수 개수에 구애 받지 않는 함수 작성
해결: *
"""

import html


# 위치 매개변수
def avg(first, *rest):
    return (first + sum(rest)) / (1 + len(rest))


# 키워드 매개변수
def make_element(name, value, **attrs):
    keyvals = [' %s="%s"' % item for item in attrs.items()]
    attr_str = ''.join(keyvals)
    element = '<{name}{attrs}>{value}</{name}>'.format(name=name, attrs=attr_str, value=html.escape(value))
    return element


print(avg(1, 2, 3, 4))
print(make_element('item', 'Albatross', size='large', quantity=6))
print(make_element('p', '<spam>'))


# 동시에
def anyargs(*args, **kwargs):
    print(args)
    print(kwargs)

