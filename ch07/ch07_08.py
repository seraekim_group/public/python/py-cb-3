"""
7.8 인자를 N개 받는 함수를 더 적은 인자로 사용
해결: functions.partial
"""

from functools import partial


def spam(a, b, c, d):
    print(a, b, c, d)


s1 = partial(spam, 1)
s1(2, 3, 4)

s2 = partial(spam, d=42)
s2(1, 2, 3)

s3 = partial(spam, 1, 2, d=42)
s3(3)

points = [(1, 2), (3, 4), (5, 6), (7, 8)]

import math


def distance(p1, p2):
    x1, y1 = p1
    x2, y2 = p2
    return math.hypot(x2 - x1, y2 - y1)


pt = (4, 3)
# sort는 인자가 하나인 함수에만 동작
points.sort(key=partial(distance, pt))
# points.sort(key=lambda p: distance(pt, p))
print(points)


def output_result(result, log=None):
    if log is not None:
        log.debug('Got: %r', result)


def add(x, y):
    return x + y


if __name__ == '__main__':
    import logging
    from multiprocessing import Pool
    from functools import partial

    logging.basicConfig(level=logging.DEBUG)
    log = logging.getLogger('test')

    p = Pool()
    p.apply_async(add, (3, 4), callback=partial(output_result, log=log))
    # p.apply_async(add, (3, 4), callback=lambda result: output_result(result, log))
    p.close()
    p.join()

