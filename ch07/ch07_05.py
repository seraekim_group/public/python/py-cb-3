"""
7.5 기본 인자를 사용하는 함수 정의
"""


def spam(a, b=42):
    print(a, b)



def spam2(a, b=None):
    if b is None:
        b = []


_no_value = object()
def spam3(a, b=_no_value):
    if b is _no_value:
        print('No b value supplied')

spam3(1)
spam3(1, None)

# 기본값은 함수를 정의할 때 정해지고 그 후로 바뀌지 못한다.
# def spam(a, b=[]) x
