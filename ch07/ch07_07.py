"""
7.7 이름 없는 함수에서 변수 고정
해결: 기본값
"""

x = 10
a = lambda y: x + y
x = 20
b = lambda y: x + y

print(a(10))
print(b(10))

c = lambda y, x=x: x + y
x = 50
d = lambda y, x=x: x + y
print(c(10))
print(d(10))

funcs = [lambda x: x+n for n in range(5)]
for f in funcs:
    print(f(0))

funcs2 = [lambda x, n=n: x+n for n in range(5)]
for f in funcs2:
    print(f(0))


