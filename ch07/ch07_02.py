"""
7.2 키워드 매개 변수만 받는 함수 작성
해결: 매개변수를 *뒤에 넣거나, 이름없이 *만 사용
"""


def mininum(*values, clip=None):
    'get min'
    m = min(values)
    if clip is not None:
        m = clip if clip > m else m
    return m


print(mininum(1, 5, 2, -5, 10))
print(mininum(1, 5, 2, -5, 10, clip=0))
help(mininum)
