"""
7.6 이름 없는 함수와 인라인 함수 정의
문제: in line 짧은 함수를 만들고 싶다.
해결: lambda
"""

add = lambda x, y: x + y
print(add(2, 3))
print(add('hel', 'wor'))

names = ['D B', 'B J', 'R H', 'N B']
print(sorted(names, key=lambda name: name.split()[-1].lower()))

