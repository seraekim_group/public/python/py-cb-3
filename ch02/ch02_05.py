"""
2.5 텍스트 검색과 치환
문제: 문자열에서 텍스트 패턴을 검색하고 치환하고 싶다.
해결: str.replace(), re.sub()
"""

text = 'Today is 11/27/2012. pcon start 3/13/2013'
import re
datepat = re.compile(r'(\d+)/(\d+)/(\d+)')
print(re.sub(r'(\d+)/(\d+)/(\d+)', r'\3-\1-\2', text))

# 치환 콜백
from calendar import month_abbr
def change_date(m):
    mon_name = month_abbr[int(m.group(1))]
    return '{} {} {}'.format(m.group(2), mon_name, m.group(3))


print(datepat.sub(change_date, text))

# 치환 횟수 확인
newtext, n = datepat.subn(r'\3-\1-\2', text)
print(n)

