"""
2.9 유니코드 텍스트 노멀화
문제: 유니코드 문자열 작업을 하고 있다. 모든 문자열에 동일한 표현식을 갖도록 보장하고 싶다.
해결: unicodedata.normalize
"""

s1 = 'Spicy Jalape\u00f1o'
# s1 = 'Spicy Jalape\xf1o'
s2 = 'Spicy Jalapen\u0303o'
print(s1)
print(s2)
print(len(s1))
print(len(s2))

import unicodedata
# 문자를 정확히 구성하도록
t1 = unicodedata.normalize('NFC', s1)
t2 = unicodedata.normalize('NFC', s2)
print(t1 == t2)
print(ascii(t1))

# 문자를 여러개로 구성
t1 = unicodedata.normalize('NFD', s1)
t2 = unicodedata.normalize('NFD', s2)
print(t1 == t2)
print(ascii(t1))

s = '\ufb01'  # 단일 문자
print(s)
print(unicodedata.normalize('NFD', s))
print(unicodedata.normalize('NFKD', s))
print(unicodedata.normalize('NFKC', s))

# 발음 구별 부호 제거
t1 = unicodedata.normalize('NFD', s1)
print(''.join(c for c in t1 if not unicodedata.combining(c)))  # combining 결합 문자인지 확인
