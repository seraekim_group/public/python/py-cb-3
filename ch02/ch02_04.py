"""
2.4 텍스트 패턴 매칭과 검색
문제: 특정 패턴에 대한 텍스트 매칭이나 검색을 하고 싶다.
해결: find, endswith, startswidth, re
"""

text = 'yeah, but no, but yeah, but no'
print(text.find('no'))

text1 = '11/27/2012'
text2 = 'Nov 27, 2012'

import re

if re.match(r'\d+/\d+/\d+', text1):
    print('yes')
else:
    print('no')

# 재활용
datepat = re.compile(r'\d+/\d+/\d+')
if datepat.match(text1):
    print('yes')
else:
    print('no')

# match는 문자열 처음에서 찾기 시도, 전체는 findall()
text = 'Today is 11/27/2012. pcon start 3/13/2013'
print(datepat.findall(text))

# 캡처 그룹
datepat = re.compile(r'(\d+)/(\d+)/(\d+)')
m = datepat.match('11/27/2012')
print(m.group(0))
print(m.group(1))
print(m.group(2))
print(m.group(3))
print(m.groups())

find_all = datepat.findall(text)
print(find_all)

for month, day, year in datepat.findall(text):
    print('{}-{}-{}'.format(year, month, day))

# re.compile을 활용하면 최근 컴파일한 패턴을 기억하기에 성능이 조금은 낫다
