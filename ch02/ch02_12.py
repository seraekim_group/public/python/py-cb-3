"""
2.12 텍스트 정리
문제: 당신의 웹 페이지에 어떤 사람이 장난스럽게 유니코드 문자를 입력했다. 이를 정리하고 싶다.
해결:
"""

s = 'pýthőň\fis\tawesome\r\n'
print(s)

remap = {
    ord('\t'): ' ',
    ord('\f'): ' ',
    ord('\r'): None  # 삭제
}

a = s.translate(remap)
print(a)

import unicodedata
import sys
cmb_chrs = dict.fromkeys(c for c in range(sys.maxunicode) if unicodedata.combining(chr(c)))
# print(cmb_chrs)
b = unicodedata.normalize('NFD', a)
print(b)
print(b.translate(cmb_chrs))

digitmap = {c: ord('0') + unicodedata.digit(chr(c)) for c in range(sys.maxunicode)
            if unicodedata.category(chr(c)) == 'Nd'}

len(digitmap)

# 아라비아 숫자
x = '\u0661\u0662\u0663'
print(x.translate(digitmap))

# 아스키 인코딩 디코딩, 아스키 표현식에만...
a = 'pýthőň is awesome\n'
b = unicodedata.normalize('NFD', a)
print(b.encode('ascii', 'ignore').decode('ascii'))
