"""
2.7 가장 짧은 매칭을 위한 정규 표현식
문제: 텍스트에서 가장 긴 부분이 매치될 때 가장 짧은 부분을 찾고 싶다면?
해결: ?를 가지고서 greedy를 lazy로
"""

import re
str_pat = re.compile(r'\"(.*)\"')
text1 = 'Computer says "no."'
print(str_pat.findall(text1))

text2 = 'Computer says "no." Phone says "yes."'
print(str_pat.findall(text2))


str_pat = re.compile(r'\"(.*?)\"')
print(str_pat.findall(text2))
