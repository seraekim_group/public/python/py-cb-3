"""
2.11 문자열에서 문자 잘라내기
문제: 텍스트의 처음, 끝, 중간에서 원하지 않는 공백문 등을 잘라내고 싶다.
해결: strip, lstrip, rstrip
"""

print('  hell world \n'.rstrip())
t = '---------hell========'
print(t.lstrip('-'))
print(t.strip('-='))

# 중간의 공백을 지우려면
s = 'hello          world'

import re

print(re.sub('\s+', ' ', s))

with open('./ch02_11.py', 'r', encoding='UTF-8') as f:
    # 데이터 변환, 실질적인 임시 리스트로 만들지 않아 효율적
    # 잘라내기 작업이 적용된 라인을 순환하는 이터레이터를 생성할 뿐
    lines = (line.strip() for line in f)
    for line in lines:
        print(line)


