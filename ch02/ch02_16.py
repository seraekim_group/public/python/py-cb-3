"""
2.16 텍스트 열의 개수 고정
문제: 긴 문자열의 서식을 바꿔 열의 개수를 조절하고 싶다.
해결: textwrap
"""

s = """Look into my ese, look into my ese, the eyes, the eyse,
the eyes, not around the eyes, don't look around the eyes,
look into my eyes, you're under.
"""

import textwrap
print(textwrap.fill(s, 70))
print(textwrap.fill(s, 30))
print(textwrap.fill(s, 30, initial_indent='        '))
print(textwrap.fill(s, 30, subsequent_indent='        '))

# 터미널에 사용할 텍스트로 알맞다. 터미널 크기 얻으려면
import os
print(os.get_terminal_size().columns)
