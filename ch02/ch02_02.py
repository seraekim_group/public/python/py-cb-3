"""
2.2 문자열 처음이나 마지막에 텍스트 매칭
문제: 파일 확장자, URL scheme 등 특정 텍스트 패턴이 포함되었는지 검사하고 싶다.
해결: str.startswith(), str.endswith()
"""

filename = 'spam.txt'
print(filename.endswith('.txt'))
print(filename.startswith('file:'))

# 여러 개의 선택지를 검사해야 한다면
import os
filenames = os.listdir('.')
print(filenames)
print([name for name in filenames if name.endswith(('.c', '.py'))])

# 입력값이 list 나 set이면 tuple로 변환해줘야 한다
choices = ['http:', 'ftp:']
url = 'http://www.python.org'
print(url.startswith(tuple(choices)))

# slice 활용 가능하지만 가독성이 많이 떨어진다
print(filename[-4:] == '.txt')
print(url[:5] == 'http:' or url[:6] == 'https:' or url[:4] == 'ftp:')

# 정규식도 가능
import re
print(re.match('http:|https:|ftp:', url))
