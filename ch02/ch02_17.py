"""
2.17 HTML과 XML 엔터티 처리
문제: HTML, XML 엔티티를 일치하는 문자로 치환 또는 특정 특수 문자를 피하고 싶다.
해결: html.escape()
"""

s = 'Elements are written as "<tag>text</tag>".'
import html
print(s)
print(html.escape(s))

# 따옴표 남기기
print(html.escape(s, quote=False))

# 텍스트를 ASCII로 만들고, 특수문자를 아스키가 아닌 문자에 끼워 넣고 싶으면
s = 'Spicy Jalapeño'
print(s.encode('ascii', errors='xmlcharrefreplace'))

s = 'Spicy &quot;Jalape&#241;o&quot;.'
from html.parser import HTMLParser
p = HTMLParser()
print(p.unescape(s))
print(html.unescape(s))

t = 'The prompt is &gt;&gt;&gt;'
from xml.sax.saxutils import unescape
print(unescape(t))
