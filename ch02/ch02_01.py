"""
2.1 여러 구분자로 문자열 나누기
문제: 문자열을 필드로 나누고 싶지만 구분자(그리고 그 주변의 공백)가 문자열에 일관적이지 않다.
해결: re.split()
"""

import re

line = 'asdf fjdk; afed, fjek,asdf,          foo '
print(re.split(r'[;,\s]\s*', line.strip()))

# ()쓰면 capture group 이 되고, 결과에 포함 됨
fields = re.split(r'(;|,|\s)\s*', line.strip())
print(fields)

# 재출력을 위해 구분 문자가 필요한 경우
values = fields[::2]
delimiters = fields[1::2] + ['']
print(values)
print(delimiters)
print(''.join(v+d for v,d in zip(values, delimiters)))

# non capture group
print(re.split(r'(?:;|,|\s)\s*', line.strip()))
