"""
2.15 문자열에 변수 사용
문제: 문자열에 변수를 사용하고 이 변수에 맞는 값을 채우고 싶다.
해결: format()
"""

s = '{name} has {n} messages. {s}'
print(s.format(name='Guido', n=37, s=s))

name = 'Guido'
n = 37
print(vars())
print(s.format_map(vars()))


class Info:
    def __init__(self, name, n='{n}'):
        self.name = name
        self.n = n
        self.s = 'hell'


a = Info('Guido', 37)
print(s.format_map(vars(a)))


# format, format_map 빠진 값이 있으면 제대로 동작 안함
class safesub(dict):
    def __missing__(self, key):
        return '[' + key + ']'


del n
print(s.format_map(safesub(vars())))

# frame hack
import sys

def sub(text):
    return text.format_map(safesub(sys._getframe(1).f_locals))

n = 37
print(sub('hello {name}'))
print(sub('You have {n} messages'))
print(sub('Your favourite color is {color}'))

# 템플릿 사용
import string
s = string.Template('$name has $n messages.')
print(s.substitute(vars()))
