"""
2.6 대소문자를 구별하지 않는 검색과 치환
문제: 텍스트를 검색하고 치환할 때 대소문자를 구별하지 않고 싶다.
해결: re.IGNORECASE 플래그 지정
"""

import re
text = 'UPPER PYTHON, lower python, Mixed Python'
print(re.findall('python', text, flags=re.IGNORECASE))
# 대소문자 불일치
print(re.sub('python', 'snake', text, flags=re.IGNORECASE))


def matchcase(word):
    def replace(m):
        text = m.group()
        if text.isupper():
            return word.upper()
        elif text.islower():
            return word.lower()
        elif text[0].isupper():
            return word.capitalize()
        else:
            return word
    return replace


print(re.sub('python', matchcase('snake'), text, flags=re.IGNORECASE))

# 유니코드 작업하기에는 부족할 수도. 2.10절 참고
