"""
2.20 바이트 문자열에 텍스트 연산 수행
문제: 바이트 문자열에 텍스트 연산(잘라내기, 검색, 치환) 하기
해결: 이미 대부분 연산을 내장함
"""

data = b'Hello World'
print(data[0:5])
print(data.startswith(b'Hello'))
print(data.split())
print(data.replace(b'Hello', b'Hell'))
print()

# 바이트 배열
data = bytearray(b'Hello World')
print(data[0:5])
print(data.startswith(b'Hello'))
print(data.split())
print(data.replace(b'Hello', b'Hell'))
print()

# 바이트 문자열 패턴 매칭
data = b'FOO:BAR,SPAM'
import re
print(re.split(b'[:,]', data))

# 주의점
# 1. 바이트 문자열에 인덱스 사용 시 정수를 가리킴
print(data[0])

# 2. 깔끔한 출력을 위해 문자열로 변환
print(data.decode('ascii'))

# 3. format 지원이 안되므로, 문자열과 인코딩 사용
# b'{} {} {}'.format(b'ACME', 100, 490.1)
print('{:10s} {:10d} {:10.2f}'.format('ACME', 100, 490.1).encode('ascii'))

# 4. 파일명으로 바이트 문자열 제공 시, 인코딩/디코딩 사용 불가

# 결론 : 바이트 문자열은 올바른 선택이 아니다.
