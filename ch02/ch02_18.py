"""
2.18 텍스트 토큰화
문제: 문자열을 파싱해서 토큰화하고 싶다.
해결: re pattern ?P<TOKEN>
"""

text = 'foo = 23 + 42 * 10'

import re
NAME = r'(?P<NAME>[a-zA-Z_][a-zA-Z_0-9]*)'
NUM = r'(?P<NUM>\d+)'
PLUS = r'(?P<PLUS>\+)'
TIMES = r'(?P<TIMES>\*)'
EQ = r'(?P<EQ>=)'
WS = r'(?P<WS>\s+)'

master_pat = re.compile('|'.join([NAME, NUM, PLUS, TIMES, EQ, WS]))
scanner = master_pat.scanner('foo = 42')
print(scanner.match())
print(scanner.match())
print(scanner.match())
print(scanner.match())
print(scanner.match())

from collections import namedtuple

Token = namedtuple('Token', ['type', 'value'])


def generate_tokens(pat, text):
    sc = pat.scanner(text)
    for m in iter(sc.match, None):
        yield Token(m.lastgroup, m.group())


for tok in generate_tokens(master_pat, text):
    print(tok)

# 주의할 점
# 1. 매칭하지 않는 텍스트가 하나라도 있으면 스캐닝이 멈춤
# 2. 순서가 중요. 중복되는 부분이 있다면 항상 더 긴 패턴을 먼저 넣는다.
LT = r'(?P<LT><)'
LE = r'(?P<LE><=)'
EQ = r'(?P<EQ>=)'

master_pat = re.compile('|'.join([LE, LT, EQ]))

# 3. 패턴이 부분 문자열을 형성하는 경우
PRINT = r'(?P<PRINT>print)'
NAME = r'(?P<NAME>[a-zA-Z_][a-zA-Z_0-9]*)'
master_pat = re.compile('|'.join([PRINT, NAME]))
for tok in generate_tokens(master_pat, 'printer'):
    print(tok)

