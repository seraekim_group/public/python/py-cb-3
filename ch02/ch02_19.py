"""
2.19 간단한 재귀 파서 작성
문제: 문법 규칙에 따른 파싱, 추상 신택스 트리
해결: 문법의 정규 스펙 BNF / EBNF, ply
"""

from ply.lex import lex
from ply.yacc import yacc

# 토큰 리스트
tokens = ['NUM', 'PLUS', 'MINUS', 'TIMES', 'DIVIDE', 'LPAREN', 'RPAREN']

# 무시 문자
t_ignore = '\t\n'

# 토큰 스펙(정규 표현식으로)
t_PLUS = r'\+'
t_MINUS = r'-'
t_TIMES = r'\*'
t_DIVIDE = r'/'
t_LPAREN = r'\('
t_RPAREN = r'\)'


# 토큰화 함수
def t_NUM(t):
    r'\d+'
    t.value = int(t.value)
    return t


# 에러 핸들러
def t_error(t):
    print('Bad character: {!r}'.format(t.value[0]))
    t.skip(1)


# 렉서(lexer) 만들기
lexer = lex()


# 문법 규칙과 핸들러 함수
def p_expr(p):
    '''
    expr : expr PLUS term
         | expr MINUS term
    '''
    if p[2] == '+':
        p[0] = p[1] + p[3]
    elif p[2] == '-':
        p[0] = p[1] - p[3]

def p_expr_term(p):
    '''
    expr : term
    '''
    p[0] = p[1]

def p_term(p):
    '''
    term : term TIMES factor
         | term DIVIDE factor
    '''
    if p[2] == '*':
        p[0] = p[1] * p[3]
    elif p[2] == '/':
        p[0] = p[1] / p[3]

def p_term_factor(p):
    '''
    term : factor
    '''
    p[0] = p[1]

def p_factor(p):
    '''
    factor : NUM
    '''
    p[0] = p[1]

def p_factor_group(p):
    '''
    factor : LPAREN expr RPAREN
    '''
    p[0] = p[2]

def p_error(p):
    print('Syntax error')


parser = yacc()
print(parser.parse('2*(3+4)+5'))
