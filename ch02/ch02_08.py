"""
2.8 여러 줄에 걸친 정규 표현식 사용
문제: 여러 줄에 걸친 정규 표현식 매칭을 사용하고 싶다.
해결: 개행문을 패턴에 넣는다.
"""

import re
comment = re.compile(r'/\*(.*?)\*/')
comment2 = re.compile(r'/\*((?:.|\n)*?)\*/')  # (?:.|\n) 논캡처 그룹
comment3 = re.compile(r'/\*(.*?)\*/', re.DOTALL)  # .이 개행문을 포함, 문제의 소지 있음. 2.18절 참고
text1 = '/* this is a comment */'
text2 = ''' /* this is a 
multiline comment */
'''

print(comment.findall(text1))
print(comment.findall(text2))
print()
print(comment2.findall(text1))
print(comment2.findall(text2))
print()
print(comment3.findall(text2))

