"""
2.10 정규 표현식에 유니코드 사용
문제: 텍스트 프로세싱에 정규 표현식을 사용 중이다. 하지만 유니코드 문자 처리가 걱정된다.
해결:
"""

import re
num = re.compile(r'\d+')
# 아스키 숫자
print(num.match('123'))
# 아라비아 숫자
print(num.match('\u0661\u0662\u0663'))

# 아라비아 코드 페이지의 모든 문자
arabic = re.compile('[\u0600-\u06ff\u0750-\u077f\u08a0-\u08ff]+')

# 대소문자를 무시하는 매칭에 대소문자 변환을 합친 코드
pat = re.compile('stra\u00dfe', re.IGNORECASE)
s = 'straβe'
print(pat.match(s))
print(s.upper())

# 유니코드와 정규 표현식을 함께 사용하기란 사실 굉장히 어렵다.
# 꼭 함께 써야 한다면 regex 라이브러리 설치
