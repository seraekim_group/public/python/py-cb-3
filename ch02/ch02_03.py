"""
2.3 쉘 와일드카드 패턴으로 문자열 매칭
문제: Unix 셸과 동일한 와일드카드 패턴을 텍스트 매칭에 사용하고 싶다
해결: fnmatch(), fnmatchcase()
"""

from fnmatch import fnmatch, fnmatchcase
print(fnmatch('foo.txt', '*.txt'))
print(fnmatch('foo.txt', '?oo.txt'))
print(fnmatch('Dat45.csv', 'Dat[0-9]*'))

names = ['Dat1.csv', 'Dat2.csv', 'config.ini', 'foo.py']
print([name for name in names if fnmatch(name, 'Dat*.csv')])

# fnmatch는 os 파일 시스템과 동일한 대소문자 구문 규칙을 따른다.
# OS X, fnmatch('foo.txt', '*.TXT') False
print(fnmatch('foo.txt', '*.TXT'))

# 이런 차이점이 마음에 들지 않는다면 fnmatchcase()를 사용
print(fnmatchcase('foo.txt', '*.TXT'))

addresses = [
    '5412 N C',
    '5148 N C',
    '5800 E 5',
    '2122 N C',
    '5645 N R',
    '1060 W A',
    '4801 N B',
    '1039 W G'
]
print([addr for addr in addresses if fnmatchcase(addr, '* C')])
print([addr for addr in addresses if fnmatchcase(addr, '54[0-9[0-9]*')])
