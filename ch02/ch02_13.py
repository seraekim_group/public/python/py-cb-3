"""
2.13 텍스트 정렬
문제: 텍스트를 특정 형식에 맞추어 정렬하고 싶다.
해결: ljust, rjust, center
"""

text = 'hell world'
print(text.ljust(20))
print(text.rjust(20))
print(text.center(20, '*'))

# format 정렬
print(format(text, '>20'))
print(format(text, '<20'))
print(format(text, '^20'))
print(format(text, '=^20s'))

print('{:>10s} {:>10s}'.format('hell', 'world'))
x = 1.2345
print(format(x, '>10'))
print(format(x, '^10.2f'))

# 오래된 코드에는 %연산자
print('%-20s ' % text)
print('%20s ' % text)
