"""
2.14 문자열 합치기
문제: 작은 문자열 여러 개를 합쳐 하나의 긴 문자열을 만들고 싶다.
해결: 문자열이 시퀀스나 순환 객체 안에 있다면 join()이 가장 빠르다.
"""

parts = ['ls', 'Chicago', 'Not', 'Chicago?']
print(' '.join(parts))

# 데이터 시퀀스 - 리스트, 튜플, 딕셔너리, 파일, 세트, 생성자 등

# 문자열 수가 아주 적다면 +
print('Is' + ' ' + 'Chicago')

# + 연산자는 복잡한 연산에서도 잘 동작
print('{} {}'.format('Is', 'Chicago'))

# 그냥 바로 합치는 거면 그냥 늘어놔도 된다
a = 'Hello' 'World'
print(a)

# +를 쓰는 것은 메모리 복사와 garbage collection 으로 인해 비효율적
s = ''
for p in parts:
    s += p

print(':'.join(['a', 'b', 'c']))
print('a', 'b', 'c', sep=':')  # 좋은 방식

# f.write(c1 + c2) 문자열 합치기
# f.write(c1); f.write(c2) 개별 입출력, 문자열이 너무 커지면 개별 입출력이 낫다


# 수많은 짧은 문자열, yield 생성자 함수 고려
# 흥미로운 점은 조각을 어떻게 조립할지 어떠한 가정도 하지 않았다는
def sample():
    yield 'Is'
    yield 'Chicago'
    yield 'Not'
    yield 'Chicago?'


text = ''.join(sample())
print(text)

# I/O redirect
# for part in sample():
#     f.write(part)

# I/O hybrid
def combine(source, maxsize):
    parts = []
    size = 0
    for part in source:
        parts.append(part)
        size += len(part)
        if size > maxsize:
            yield ''.join(parts)
            parts = []
            size = 0
    yield ''.join(parts)

# for part in combine(sample(), 32768):
#     f.write(part)

