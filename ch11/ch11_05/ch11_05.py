"""
11.5 간단한 REST 기반 인터페이스 생성
해결: WSGI 기반 라이브러리 생성
"""


import time

_hello_resp = '''\
<html>
    <head><title>Hello {name}</title></head>
    <body><h1>Hello {name}!</h1></body>
</html>
'''

def hello_world(env, start_res):
    start_res('200 OK', [('Content-type', 'text/html')])
    params = env['params']
    resp = _hello_resp.format(name = params.get('name'))
    yield resp.encode('utf-8')


_localtime_resp = '''\
<?xml version="1.0"?>
<time>
<year>{t.tm_year}</year>
<month>{t.tm_mon}</month>
<day>{t.tm_mday}</day>
<hour>{t.tm_hour}</hour>
<minute>{t.tm_min}</minute>
<second>{t.tm_sec}</second>
</time>
'''

def localtime(env, start_res):
    start_res('200 OK', [('Content-type', 'text/xml')])
    resp = _localtime_resp.format(t=time.localtime())
    yield resp.encode('utf-8')


if __name__ == '__main__':
    from ch11.ch11_05.resty import PathDispatcher
    from wsgiref.simple_server import make_server

    # 디스패처를 생성하고 함수 등록
    dispatcher = PathDispatcher()
    dispatcher.register('GET', '/hello', hello_world)
    dispatcher.register('GET', '/localtime', localtime)

    # 기본 서버 실행
    httpd = make_server('', 8080, dispatcher)
    print('Serving on port 8080...')
    httpd.serve_forever()

