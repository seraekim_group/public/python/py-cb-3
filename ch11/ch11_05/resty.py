import cgi


def notfound_404(env, start_res):
    start_res('404 Not Found', [('Content-type', 'text/plain')])
    return [b'Not Found']

class PathDispatcher:
    def __init__(self):
        self.pathmap = {}

    def __call__(self, env, start_res):
        path = env['PATH_INFO']
        params = cgi.FieldStorage(env['wsgi.input'], environ=env)
        method = env['REQUEST_METHOD'].lower()
        env['params'] = {key: params.getvalue(key) for key in params}
        handler = self.pathmap.get((method, path), notfound_404)
        return handler(env, start_res)

    def register(self, method, path, function):
        self.pathmap[method.lower(), path] = function
        return function
