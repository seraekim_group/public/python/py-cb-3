"""
11.4 CIDR 주소로 IP 주소 생성
해결: ipaddress
"""

import ipaddress
net = ipaddress.ip_network('192.168.211.0/30')
for a in net:
    print(a)

net6 = ipaddress.ip_network('fe80::7415:4d33:cff1:0/125')
for a in net6:
    print(a)

print(net.num_addresses)
print(net6.num_addresses)
print(ipaddress.ip_address('123.45.4.2') in net)

inet = ipaddress.ip_interface('192.168.211.0/30')
print(inet.network)
print(inet.netmask)
print(inet.ip)
