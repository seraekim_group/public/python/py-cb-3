"""
11.1 클라이언트로 HTTP 서비스와 통신
해결: urllib.request, requests
"""

from urllib import request, parse

url = 'http://httpbin.org/get'

params = {
    'name1': 'value1',
    'name2': 'value2',
}


querystring = parse.urlencode(params)

u = request.urlopen(url+'?'+querystring)
resp = u.read()
print(resp)

# post 라면
url = 'http://httpbin.org/post'
u = request.urlopen(url, querystring.encode('ascii'))
resp = u.read()
print(resp)

# 헤더추가라면..

headers = {
    'User-agent': 'none/ofyourbusiness',
    'Spam': 'Eggs'
}

req = request.Request(url, querystring.encode('ascii'), headers=headers)
u = request.urlopen(req)
resp = u.read()
print(resp)

# 더 복잡한 경우, requests 활용
import requests
resp = requests.post(url, data=params, headers=headers)
print(resp.text)
print(resp.content)
print(resp.json)

resp = requests.head('http://www.python.org/index.html')
status = resp.status_code

print(status, resp.headers)

# 로그인
resp = requests.get('http://pypi.python.org/pypi?:action=login', auth=('user', 'password'))
print(resp)

# 쿠키
url = 'https://google.com'
resp1 = requests.get(url)
print(resp1.cookies)
resp2 = requests.get(url, cookies=resp1.cookies)
print(resp2.cookies)

# 데이터 업로드
url = 'http://httpbin.org/post'
files = {'file': {'ch11_01.py', open('ch11_01.py', 'rb')}}
r = requests.post(url, files=files)
print(r.json)
