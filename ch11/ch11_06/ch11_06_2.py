from xmlrpc.client import ServerProxy


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y


s = ServerProxy('http://localhost:15000', allow_none=True)
s.set('foo', 'bar')
s.set('spam', [1, 2, 3])
s.set('point', Point(1,2))
s.set('bin', b'hell')
print(s.keys())
print(s.get('foo'))
print(s.get('spam'))
print(s.get('point'))
print(s.get('bin'))
s.delete('spam')
print(s.exists('spam'))
