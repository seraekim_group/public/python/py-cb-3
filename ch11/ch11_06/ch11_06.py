"""
11.6 XML-RPC로 간단한 원격 프로시저 호출 구현
문제: 원격 머신에서 실행 중인 파이썬 프로그램의 함수나 메소드를 호출하고 싶다.
해결: XML-RPC 구현
"""

from xmlrpc.server import SimpleXMLRPCServer

class KeyValueServer:
    _rpc_methods = ['get', 'set', 'delete', 'exists', 'keys']
    def __init__(self, address):
        self._data = {}
        self._serv = SimpleXMLRPCServer(address, allow_none=True)
        for name in self._rpc_methods:
            self._serv.register_function(getattr(self, name))

    def get(self, name):
        return self._data[name]

    def set(self, name, value):
        self._data[name] = value

    def delete(self, name):
        del self._data[name]

    def exists(self, name):
        return name in self._data

    def keys(self):
        return list(self._data)

    def serve_forever(self):
        self._serv.serve_forever()


if __name__ == '__main__':
    kvserv = KeyValueServer(('', 15000))
    kvserv.serve_forever()

