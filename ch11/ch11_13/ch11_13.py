"""
11.13 큰 배열 보내고 받기
해결: memoryview
"""


def send_from(arr, dest):
    view = memoryview(arr).cast('B')
    while len(view):
        nsent = dest.send(view)
        view = view[nsent:]

from socket import *
s = socket(AF_INET, SOCK_STREAM)
s.bind(('', 25000))
s.listen(1)
c,a = s.accept()

import numpy
a = numpy.arange(0.0, 50000000.0)
send_from(a, c)
