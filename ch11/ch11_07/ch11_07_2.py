from multiprocessing.connection import Client

c = Client(('localhost', 25000), authkey=b'peekaboo')


def send(msg):
    c.send(msg)
    print(c.recv())


send('hello')
send(42)
send([1, 2, 3, 4, 5])



