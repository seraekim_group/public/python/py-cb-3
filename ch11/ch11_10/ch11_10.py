"""
11.10 네트워크 서비스에 SSL 추가
"""

from socket import socket, AF_INET, SOCK_STREAM
import ssl

KEYFILE = 'server_key.pem'
CERTFILE = 'server_cert.pem'


def echo_client(s):
    while True:
        data = s.recv(8192)
        if data == 'b':
            break
        s.send(data)
        print('Connection closed')


def echo_server(address):
    s = socket(AF_INET, SOCK_STREAM)
    s.bind(address)
    s.listen(1)

    # 클라이언트 인증을 요구하는 SSL 레이어로 감싼다.
    s_ssl = ssl.wrap_socket(s, keyfile=KEYFILE, certfile=CERTFILE, server_side=True)

    # 연결 기다림
    while True:
        try:
            c,a = s_ssl.accept()
            print('Got connection', c, a)
            echo_client(c)
        except Exception as e:
            print('{}: {}'.format(e.__class__.__name__, e))

echo_server(('', 20000))
