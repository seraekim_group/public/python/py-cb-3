"""
11.8 원격 프로시저 호출 구현
문제: 소켓, multiprocessing, ZeroMQ와 같은 message passing layer 상단에 RPC를 구현
해결: pickle
"""

from multiprocessing.connection import Listener
from threading import Thread
from ch11.ch11_08.rpcserver import RPCHandler

def rpc_server(handler, address, authkey):
    sock = Listener(address, authkey=authkey)
    while True:
        client = sock.accept()
        t = Thread(target=handler.handle_connection, args=(client,))
        t.daemon = True
        t.start()


# 원격 함수
def add(x, y):
    return x + y


def sub(x, y):
    return x - y


# 핸들러에 등록
handler = RPCHandler()
handler.register_function(add)
handler.register_function(sub)

# 서버 실행
rpc_server(handler, ('localhost', 17000), authkey=b'peekaboo')
