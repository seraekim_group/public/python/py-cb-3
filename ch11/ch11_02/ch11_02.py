"""
11.2 TCP 서버 만들기
해결: socketserver
"""

from socketserver import BaseRequestHandler, TCPServer

class EchoHandler(BaseRequestHandler):
    def handle(self):
        print('Got connection from', self.client_address)
        while True:
            msg = self.request.recv(8192)
            if not msg:
                break
            self.request.send(msg)


# 다른 예제
from socketserver import StreamRequestHandler, ThreadingTCPServer
import socket

class EchoHandler2(StreamRequestHandler):
    timeout = 5
    rbufsize = -1
    wbufsize = 0
    disable_nagle_algorithm = False
    def handle(self):
        print('Got connection from', self.client_address)
        try:
            for line in self.rfile:
                self.wfile.write(line)
        except socket.timeout:
            print('Timed out!')


if __name__ == '__main__':
    TCPServer.allow_reuse_address = True
    serv = TCPServer(('', 20000), EchoHandler)
    serv.serve_forever()

# if __name__ == '__main__':
#     from threading import Thread
#     NWORKERS = 16
#     TCPServer.allow_reuse_address = True
#     serv = TCPServer(('', 20000), EchoHandler2)
#     for n in range(NWORKERS):
#         t = Thread(target=serv.serve_forever)
#         t.daemon = True
#         t.start()
#     serv.serve_forever()

