"""
8.1 인스턴스의 문자열 표현식 변형
해결: __str__(), __repr__()
"""


class Pair:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return 'Pair({0.x!r}, {0.y!r})'.format(self)
        # return 'Pair(%r, %r)' % (self.x, self.y)

    def __str__(self):
        return '({0.x!s}, {0.y!s})'.format(self)


p = Pair(3, 4)
print('p is {0!r}'.format(p))
print('p is {0}'.format(p))

# __repr__() , eval(repr(x)) == x, 이것이 안 될때, <>사이에 텍스트 넣음
# __str__() 정의 안하면, __repr__()을 사용함
# {0.x} 는 인자0의 x속성 0 = self instance