"""
8.17 init 호출 없이 인스턴스 생성
해결: __new__()
"""

import time


class Date:
    def __init__(self, y, m, d):
        self.y = y
        self.m = m
        self.d = d

    @classmethod
    def today(cls):
        d = cls.__new__(cls)
        t = time.localtime()
        d.y = t.tm_year
        d.m = t.tm_mon
        d.d = t.tm_mday
        return d


d = Date.__new__(Date)
data = {'y': 1989, 'm': 8, 'd': 12}
for k, v in data.items():
    setattr(d, k, v)

print(d.y)
