"""
8.25 캐시 인스턴스 생성
해결: factory function, 이미 logging 등에 쓰이고 있음
"""

import logging
a = logging.getLogger('foo')
b = logging.getLogger('bar')
c = logging.getLogger('foo')
print(a is b)
print(a is c)


class Spam:
    def __init__(self, name):
        self.name = name


# 캐시 지원
import weakref
_spam_cache = weakref.WeakValueDictionary()


def get_spam(name):
    if name not in _spam_cache:
        s = Spam(name)
        _spam_cache[name] = s
    else:
        s = _spam_cache[name]
    return s


a = get_spam('foo')
b = get_spam('bar')
c = get_spam('foo')
print(a is b)
print(a is c)
print(list(_spam_cache))  # 아이템이 존재하는 경우만 담는다
del a
del c
print(list(_spam_cache))


# 전역변수, 팩토리 함수에 너무 의존하고 있음. 캐싱 코드를 별도의 관리 클래스로...
class CachedSpamManager:
    def __init__(self):
        self._cache = weakref.WeakValueDictionary()

    def get_spam(self, name):
        if name not in self._cache:
            s = Spam2._new(name)
            self._cache[name] = s
        else:
            s = self._cache[name]
        return s

    def clear(self):
        self._cache.clear()


class Spam2:
    manager = CachedSpamManager()

    def __init__(self, *args, **kwargs):
        raise RuntimeError("Can't instantiate directly")

    @classmethod
    def _new(cls, name):
        self = cls.__new__(cls)
        self.name = name
        return self


# def get_spam2(name):
#     return Spam2.manager.get_spam()

a = Spam2.manager.get_spam('foo')
b = Spam2.manager.get_spam('bar')
c = Spam2.manager.get_spam('foo')
# c = Spam2._new('foo')
print(a is c)
