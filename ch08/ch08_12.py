"""
8.12 인터페이스, 추상 베이스 클래스 정의
해결: abc
"""


from abc import ABCMeta, abstractmethod


class IStream(metaclass=ABCMeta):
    @abstractmethod
    def read(self, maxbytes=-1):
        pass

    @abstractmethod
    def write(self, data):
        pass


# 추상 베이스 클래스는 직접 인스턴스화 못함
# x IStream()
class SocketStream(IStream):
    def read(self, maxbytes=-1):
        pass

    def write(self, data):
        pass


# 인터페이스 명시적 확인하기
def serialize(obj, stream):
    if not isinstance(stream, IStream):
        raise TypeError('Expected an IStream')


# 우리의 인터페이스를 내장IO 등록
import io
IStream.register(io.IOBase)
print(isinstance(open('ch08_12.py'), IStream))

from abc import ABCMeta, abstractmethod


class A(metaclass=ABCMeta):
    @property
    @abstractmethod
    def name(self):
        pass

    @name.setter
    @abstractmethod
    def name(self, value):
        pass

    @classmethod
    @abstractmethod
    def method1(cls):
        pass

    @staticmethod
    @abstractmethod
    def method2():
        pass

# 파이썬은 동적 타입 지원하는데 타입 확인을 너무 남용하면 복잡해진다.
from decimal import Decimal
import numbers

x = Decimal('3.4')
print(isinstance(x, numbers.Real))  # 소수이니 True 라고 예상되지만 False 리턴
