"""
8.4 인스턴스를 많이 생성할 때 메모리 절약
해결: __slots__, 단, slots 명시자에 나열한 속성만 사용할 수 있으며, 추가가 불가능
"""


# slots 없을 때, 428바이트 소비, 있을 때 156바이트 소비
# 수백만 개의 인스턴스를 생성하는 경우가 아닌이상, slots 사용은 지양
class Date:
    __slots__ = ['year', 'month', 'day']

    def __init__(self, year, month, day):
        self.year = year
        self.month = month
        self.day = day
