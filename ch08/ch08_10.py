"""
8.10 게으른 계산을 하는 프로퍼티 사용
해결: 디스크립터
"""

# 수정 허용
# class lazyproperty:
#     def __init__(self, func):
#         self.func = func
#
#     def __get__(self, instance, owner):
#         if instance is None:
#             return self
#         else:
#             value = self.func(instance)
#             setattr(instance, self.func.__name__, value)
#             return value


# 수정을 비허용
# 이 버전의 경우 getter를 써야한다는 비효율
def lazyproperty(func):
    name = '_lazy_' + func.__name__
    @property
    def lazy(self):
        if hasattr(self, name):
            return getattr(self, name)
        else:
            value = func(self)
            setattr(self, name, value)
            return value
    return lazy


import math


class Circle:
    def __init__(self, radius):
        self.radius = radius

    @lazyproperty
    def area(self):
        print('Computing area')
        return math.pi * self.radius ** 2

    @lazyproperty
    def perimeter(self):
        print('Computing perimeter')
        return 2 * math.pi * self.radius


c = Circle(4.0)
print(vars(c))
print(c.radius)
print(c.area)  # computing
print(vars(c))
print(c.area)
print(c.perimeter)  # computing
print(vars(c))
print(c.perimeter)

# 변수를 삭제하면 다시 실행됨
del c.area
print('삭제 후')
print(c.area)

# 직접 접근하여 수정 시
c.area = 25
print(c.area)

