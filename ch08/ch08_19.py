"""
8.19 상태 객체 혹은 상태 기계 구현
"""


class Connection:
    def __init__(self):
        self.state = 'CLOSED'

    def read(self):
        if self.state != 'OPEN':
            raise RuntimeError('Not open')
        print('reading')

    def write(self, data):
        if self.state != 'OPEN':
            raise RuntimeError('Not open')
        print('writing')

    def open(self):
        if self.state == 'OPEN':
            raise RuntimeError('Already open')
        self.state = 'OPEN'

    def close(self):
        if self.state == 'CLOSED':
            raise RuntimeError('Already closed')
        self.state = 'CLOSED'


# 모든 상태는 스태틱, 인스턴스 데이터는 connection2에
class Connection2:
    def __init__(self):
        self.new_state(ClosedConnectionState)

    def new_state(self, newstate):
        self._state = newstate

    # 상태 클래스로 델리게이트
    def read(self):
        return self._state.read(self)

    def write(self, data):
        return self._state.write(self, data)

    def open(self):
        return self._state.open(self)

    def close(self):
        return self._state.close(self)


# 연결 상태 베이스 클래스
class ConnectionState:
    @staticmethod
    def read(conn):
        raise NotImplementedError('Not open')

    @staticmethod
    def write(conn, data):
        raise NotImplementedError('Not open')

    @staticmethod
    def open(conn):
        raise NotImplementedError()

    @staticmethod
    def close(conn):
        raise NotImplementedError()


# 여러 상태 구현
class ClosedConnectionState(ConnectionState):
    @staticmethod
    def read(conn):
        raise RuntimeError('Not open')

    @staticmethod
    def write(conn, data):
        raise RuntimeError('Not open')

    @staticmethod
    def open(conn):
        conn.new_state(OpenConnectionState)

    @staticmethod
    def close(conn):
        raise RuntimeError('Already closed')


class OpenConnectionState(ConnectionState):
    @staticmethod
    def read(conn):
        print('reading')

    @staticmethod
    def write(conn, data):
        print('writing')

    @staticmethod
    def open(conn):
        raise RuntimeError('Already open')

    @staticmethod
    def close(conn):
        conn.new_state(ClosedConnectionState)


c = Connection2()
print(c._state)
c.open()
print(c._state)
c.read()
c.close()
print(c._state)


# 추가적인 간접 접근 없애기, Connection과 ConnectionState 두 클래스 합치기
# 좀 더 빠름
class Connection3:
    def __init__(self):
        self.new_state(ClosedConnection)

    def new_state(self, newstate):
        self.__class__ = newstate

    def read(self):
        raise NotImplementedError()

    def write(self, data):
        raise NotImplementedError()

    def open(self):
        raise NotImplementedError()

    def close(self):
        raise NotImplementedError()


class ClosedConnection(Connection3):
    def read(self):
        raise RuntimeError('Not open')

    def write(self, data):
        raise RuntimeError('Not open')

    def open(self):
        self.new_state(OpenConnection)

    def close(self):
        raise RuntimeError('Already closed')


class OpenConnection(Connection3):
    def read(self):
        print('reading')

    def write(self, data):
        print('writing')

    def open(self):
        raise RuntimeError('Already open')

    def close(self):
        self.new_state(ClosedConnection)


c = Connection3()
print(c.__class__)
c.open()
print(c.__class__)
c.read()
c.close()
print(c.__class__)


# if elif else 구문에도 쓰임
class State:
    def __init__(self):
        self.state = 'A'

    def action(self, x):
        if self.state == 'A':
            print('A')
            self.state = 'B'
        elif self.state == 'B':
            print('B')
            self.state = 'C'
        elif self.state == 'C':
            print('C')
            self.state = 'A'


class State2:
    def __init__(self):
        self.new_state(State_A)

    def new_state(self, state):
        self.__class__ = state

    def action(self, x):
        raise NotImplementedError()


class State_A(State2):
    def action(self, x):
        print('A')
        self.new_state(State_B)


class State_B(State2):
    def action(self, x):
        print('B')
        self.new_state(State_C)


class State_C(State2):
    def action(self, x):
        print('C')
        self.new_state(State_A)

s = State2()
s.new_state(State_A)
s.action(1)
s.action(1)
s.action(1)

