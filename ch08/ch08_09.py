"""
8.9 새로운 클래스나 인스턴스 속성 만들기
해결: 완전히 새로운 종류의 인스턴스 속성을 만드려면, 그 기능을 디스크립터 클래스
형태로 정의해야 함.
"""


class Integer:
    def __init__(self, name):
        self.name = name

    def __get__(self, instance, owner):
        if instance is None:  # 클래스 변수인 경우
            print('class access')
            return self
        else:
            return instance.__dict__[self.name]

    def __set__(self, instance, value):
        if not isinstance(value, int):
            raise TypeError('Expected an int')
        instance.__dict__[self.name] = value

    def __delete__(self, instance):
        del instance.__dict__[self.name]


class Point:
    # 반드시 클래스 변수로..
    x = Integer('x')
    y = Integer('y')

    def __init__(self, x, y):
        self.x = x
        self.y = y


p = Point(2, 3)
print(Point.x)  # Point.x.__get__(None, Point)
print(p.x)  # Point.x.__get__(p, Point)
p.y = 5
# p.x = 2.3


# 속성 타입을 확인하는 디스크립터
class Typed:
    def __init__(self, name, expected_type):
        self.name = name
        self.expected_type = expected_type

    def __get__(self, instance, owner):
        if instance is None:
            return self
        else:
            return instance.__dict__[self.name]

    def __set__(self, instance, value):
        if not isinstance(value, self.expected_type):
            raise TypeError('Expected ' + str(self.expected_type))
        instance.__dict__[self.name] = value

    def __delete__(self, instance):
        del instance.__dict__[self.name]


# 선택한 속성에 적용되는 클래스 데코레이터
def typeassert(**kwargs):
    def decorate(cls):
        for name, expected_type in kwargs.items():
            # 클래스에 Typed 디스크립터 설정
            setattr(cls, name, Typed(name, expected_type))
        return cls
    return decorate


@typeassert(name=str, shares=int, price=float)
class Stock:
    def __init__(self, name, shares, price):
        self.name = name
        self.shares = shares
        self.price = price


# 단지 한 속성에 대한 접근이라면, 프로퍼티 사용이 낫다
Stock('str', 55, 55.3)
Stock('str', 55, 55)
