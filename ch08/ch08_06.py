"""
8.6 관리 속성 만들기
해결: property 정의
"""


class Person:
    def __init__(self, first_name):
        # 세터 메소드 사용
        self.first_name = first_name

    @property
    def first_name(self):
        return self._first_name

    @first_name.setter
    def first_name(self, value):
        if not isinstance(value, str):
            raise TypeError('Expected a string')
        self._first_name = value

    @first_name.deleter
    def first_name(self):
        raise AttributeError("Can't delete attribute")


a = Person('srkim')
print(a.first_name)
a.first_name = 'srk'
print(a.first_name)
# 함수 직접 호출
print(Person.first_name.fget)
