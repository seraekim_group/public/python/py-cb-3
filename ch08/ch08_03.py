"""
8.3 객체의 콘텍스트 관리 프로토콜 지원
해결: __enter__(), __exit__()
"""

from socket import socket, AF_INET, SOCK_STREAM
from functools import partial


class LazyConnection:
    def __init__(self, address, family=AF_INET, type=SOCK_STREAM):
        self.address = address
        self.family = AF_INET
        self.type = SOCK_STREAM
        self.connections = []
        # self.sock = None

    def __enter__(self):
        # if self.sock is not None:
        #     raise RuntimeError('Already connected')
        # self.sock = socket(self.family, self.type)
        sock = socket(self.family, self.type)
        sock.connect(self.address)
        self.connections.append(sock)
        # return self.sock
        return sock

    def __exit__(self, exc_type, exc_val, exc_tb):
        # self.sock.close()
        # self.sock = None
        self.connections.pop().close()


conn = LazyConnection(('www.python.org', 80))

# 연결 종료
with conn as s:  # enter의 반환값이 as s 로
    with conn as s2:
        pass
    # conn.__enter__() 실행: 연결
    s.send(b'GET /index.html HTTP/1.0\r\n')
    s.send(b'Host: www.python.org\r\n')
    s.send(b'\r\n')
    resp = b''.join(iter(partial(s.recv, 8192), b''))
    print(resp)
    # conn.__exit__() 실행

