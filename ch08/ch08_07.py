"""
8.7 부모 클래스의 메소드 호출
해결: super()
"""


class A:
    def __init__(self):
        self.x = 0

    def spam(self):
        print('A.spam')


class B(A):
    def __init__(self):
        super().__init__()
        self.y = 1

    def spam(self):
        print('B.spam')
        super().spam()  # 부모의 spam 호출


class Proxy:
    def __init__(self, obj):
        self._obj = obj

    # 내부 obj를 위해 델리게이트(delegate) 속성 찾기
    def __getattr__(self, name):
        return getattr(self._obj, name)

    def __setattr__(self, name, value):
        if name.startswith('_'):
            super().__setattr__(name, value)  # 원본
        else:
            setattr(self._obj, name, value)


class Base:
    def __init__(self):
        print('Base.__init__')


class A2:
    def __init__(self):
        super().__init__()
        print('A2.__init__')

    def spam(self):
        print('A2 spam')
        super().spam()


class B2(Base):
    def __init__(self):
        super().__init__()
        print('B2.__init__')

    def spam(self):
        print('B2 spam')


class C(A2, B2):
    def __init__(self):
        super().__init__()
        print('C.__init__')


c = C()

# method resolution order, MRO, C3 Linearization
print(C.__mro__)

# super엔 없는데.. super().spam()은, A.spam, B.spam 두개를 실행
c.spam()
