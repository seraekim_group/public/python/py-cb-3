"""
8.16 클래스에 생성자 여러 개 정의
해결: 클래스 메소드
"""

import time


class Date:
    def __init__(self, y, m, d):
        self.y = y
        self.m = m
        self.d = d

    # 대안 생성자
    @classmethod
    def today(cls):
        t = time.localtime()
        return cls(t.tm_year, t.tm_mon, t.tm_mday)


a = Date(1989, 8, 12)
b = Date.today()


class NewDate(Date):
    pass


c = Date.today()  # cls=Date 인스턴스 생성
d = NewDate.today()  # cls=NewDate 인스턴스 생성

# init 하나에 모든 기능 넣을 수도 있지만, 뭘 의미하는 건지 이해하기도 힘들고 코드도 복잡해짐

