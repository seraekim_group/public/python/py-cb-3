"""
8.23 순환 자료 구조에서 메모리 관리
해결: 부모-자식 트리 구조에서, 링크 중 하나를 weakref 약한 참조로 만든다.
"""

import weakref
from time import sleep

class Node:
    def __init__(self, value):
        self.value = value
        self._parent = None
        self.children = []

    def __repr__(self):
        return 'Node({!r:})'.format(self.value)

    # 부모를 약한 참조로 관리하는 프로퍼티
    @property
    def parent(self):
        return self._parent if self._parent is not None else self._parent()

    @parent.setter
    def parent(self, node):
        self._parent = weakref.ref(node)

    def add_child(self, child):
        self.children.append(child)
        child.parent = self


root = Node('parent')
c1 = Node('child')
root.add_child(c1)
print(c1.parent)
del root
print(c1.parent)
print(c1)

# 순환자료구조는 일반적인 garbage collection 규칙이 적용 안됨


# 삭제 시기를 알려주는 클래스
class Data:
    def __del__(self):
        print('Data.__del__')


# 순환 구조가 있는 Node 클래스
class Node2:
    def __init__(self):
        self.data = Data()
        self.parent = None
        self.children = []

    def add_child(self, child):
        self.children.append(child)
        child.parent = self


a = Data()
del a
a = Node2()
a_ref = weakref.ref(a)
print(a_ref())
del a
print(a_ref())
a = Node2()
a.add_child(Node2())
del a  # 삭제되지 않음, reference counting
sleep(3)

