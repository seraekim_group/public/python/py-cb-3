"""
8.15 속성 접근 델리게이팅
"""


# 단순 패턴
class A:
    def spam(self, x):
        pass

    def foo(self):
        pass


class B:
    def __init__(self):
        self._a = A()

    # 델리게이트 할 메소드가 많으면 getattr 활용
    def __getattr__(self, item):
        print('getattr 호출', item)
        return getattr(self._a, item)

    # def spam(self, x):
    #     # 내부 self._a 인스턴스로 델리게이트
    #     return self._a.spam(x)
    #
    # def foo(self):
    #     # 내부 self._a 인스턴스로 델리게이트
    #     return self._a.foo()

    def bar(self):
        pass


b = B()
b.bar()
b.spam(42)  # B에 없으므로 getattr 호출 후 A.spam으로 델리게이트


# 프록시 구현
class Proxy:
    def __init__(self, obj):
        self._obj = obj

    # 속성 검색
    def __getattr__(self, item):
        print('getattr 호출', item)
        return getattr(self._obj, item)

    # 속성 할당
    def __setattr__(self, key, value):
        if key.startswith('_'):
            super().__setattr__(key, value)
        else:
            print('setattr:', key, value)
            setattr(self._obj, key, value)

    # 속성 삭제
    def __delattr__(self, item):
        if item.startswith('_'):
            super().__delattr__(item)
        else:
            print('delattr:', item)
            delattr(self._obj, item)


class Spam:
    def __init__(self, x):
        self.x = x

    def bar(self, y):
        print('Spam.bar:', self.x, y)


s = Spam(2)
p = Proxy(s)

# 프록시에 접근
print(p.x)
p.bar(3)
p.x = 37

# 상속 대신에 델리게이트를 쓰기도 하는데, 상속하기에 애매하거나, 특정 메소드만 노출하는 등 유용

# __getattr__() 은 밑줄 두 개로 시작하는 대부분의 특별 메소드에 적용되지 않는다.
# len 등 직접 구현해야 함


class ListLike:
    def __init__(self):
        self._items = []

    def __getattr__(self, item):
        return getattr(self._items, item)

    # 특정 리스트 연산을 지원기 위한 특별 메소드 추가
    def __len__(self):
        return len(self._items)

    def __getitem__(self, item):
        return self._items[item]

    def __setitem__(self, key, value):
        self._items[key] = value

    def __delitem__(self, key):
        del self._items[key]

