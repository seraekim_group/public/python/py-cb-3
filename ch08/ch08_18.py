"""
8.18 믹스인으로 클래스 확장
"""


class LoggedMappingMixin:
    """get/set/delete 로깅 추가"""
    __slots__ = ()  # 인스턴스가 없음을 암시

    def __getitem__(self, item):
        print('Getting ' + str(item))
        return super().__getitem__(item)  # dict.__getitem__()

    def __setitem__(self, key, value):
        print('Setting {} = {!r}'.format(key, value))
        return super().__setitem__(key, value)

    def __delitem__(self, key):
        print('Deleting ' + str(key))
        return super().__delitem__(key)


class SetOnceMappingMixin:
    """키가 한 번만 설정되도록"""
    __slots__ = ()

    def __setitem__(self, key, value):
        if key in self:
            raise KeyError(str(key) + ' already set')
        return super().__setitem__(key, value)


class StringKeyMappingMixin:
    """키에 문자열만 허용"""
    __slots__ = ()

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            raise TypeError('keys must be strings')
        return super().__setitem__(key, value)


# 위 클래스들은 자체로는 쓸모가 없다. 다른 매핑 클래스와 다중 상속을 통해 혼용해야 함
class LoggedDict(LoggedMappingMixin, dict):
    pass


d = LoggedDict()
d['x'] = 23
d['x']
del d['x']

from collections import defaultdict


class SetOnceDefaultDict(SetOnceMappingMixin, defaultdict):
    pass


d = SetOnceDefaultDict(list)
d['x'].append(2)
d['x'].append(3)
d['x'].append(10)
# d['x'] = 23 KeyError: 'x already set'

from collections import OrderedDict


class StringOrderedDict(StringKeyMappingMixin, SetOnceMappingMixin, OrderedDict):
    pass


d = StringOrderedDict()
d['x'] = 23
# d[42] = 10 TypeError: keys must be strings
# d['x'] = 42 KeyError: 'x already set'

# 멀티 스레드 XMLRPC
# from xmlrpc.server import SimpleXMLRPCServer
# from socketserver import ThreadingMixIn
#
#
# class ThreadedXMLRPCServer(ThreadingMixIn, SimpleXMLRPCServer):
#     pass


# mixin 클래스에서 __init__() 정의
class RestrictKeysMixin:
    def __init__(self, *args, _restrict_key_type, **kwargs):
        self.__restrict_key_type = _restrict_key_type
        super().__init__(*args, **kwargs)

    def __setitem__(self, key, value):
        if not isinstance(key, self.__restrict_key_type):
            raise TypeError('Keys must be ' + str(self.__restrict_key_type))
        super().__setitem__(key, value)


class RDict(RestrictKeysMixin, dict):
    pass


d = RDict(_restrict_key_type=str)
e = RDict([('name', 'D'), ('n', 37)], _restrict_key_type=str)
f = RDict(name='D', n=37, _restrict_key_type=str)
print(d)
print(e)
print(f)


# 클래스 데코레이터 활용
def LoggedMapping(cls):
    cls_getitem = cls.__getitem__
    cls_setitem = cls.__setitem__
    cls_delitem = cls.__delitem__

    def __getitem__(self, key):
        print('Getting ' + str(key))
        return cls_getitem(self, key)

    def __setitem__(self, key, value):
        print('Setting {} = {!r}'.format(key, value))
        return cls_setitem(self, key, value)

    def __delitem__(self, key):
        print('Deleting ' + str(key))
        return cls_delitem(self, key)

    cls.__getitem__ = __getitem__
    cls.__setitem__ = __setitem__
    cls.__delitem__ = __delitem__
    return cls


@LoggedMapping
class LoggedDict2(dict):
    pass


d = LoggedDict2()
d['x'] = 23
d['x']
del d['x']
