"""
8.13 데이터 모델 혹은 타입 시스템 구현
해결: 디스크립터
"""


# 베이스 클래스, 디스크립터로 값을 설정
class Descriptor:
    def __init__(self, name=None, **opts):
        self.name = name
        for key, value in opts.items():
            setattr(self, key, value)

    def __set__(self, instance, value):
        instance.__dict__[self.name] = value


# 타입을 강제하기 위한 디스크립터
class Typed(Descriptor):
    expected_type = type(None)

    def __set__(self, instance, value):
        if not isinstance(value, self.expected_type):
            raise TypeError('expected ' + str(self.expected_type))
        super().__set__(instance, value)


# 값을 강제하기 위한 디스크립터
class Unsigned(Descriptor):
    def __set__(self, instance, value):
        if value < 0:
            raise ValueError('Expected >= 0')
        super().__set__(instance, value)


class MaxSized(Descriptor):
    def __init__(self, name=None, **opts):
        if 'size' not in opts:
            raise TypeError('missing size option')
        super().__init__(name, **opts)

    def __set__(self, instance, value):
        if len(value) >= self.size:
            raise ValueError('size must be < ' + str(self.size))
        super().__set__(instance, value)


class Integer(Typed):
    expected_type = int


class UnsignedInteger(Integer, Unsigned):
    pass


class Float(Typed):
    expected_type = float


class UnsignedFloat(Float, Unsigned):
    pass


class String(Typed):
    expected_type = str


class SizedString(String, MaxSized):
    pass


class Stock:
    name = SizedString('name', size=8)
    shares = UnsignedInteger('shares')
    price = UnsignedFloat('price')

    def __init__(self, name, shares, price):
        self.name = name
        self.shares = shares
        self.price = price


s = Stock('ACME', 50, 91.1)
print(s.name)
# s.shares = -10
# s.price = 'a lot'
# s.name = '123456789'


# 데코레이터 방식
def check_attributes(**kwargs):
    def decorate(cls):
        for k, v in kwargs.items():
            if isinstance(v, Descriptor):
                v.name = k
                setattr(cls, k, v)
            else:
                setattr(cls, k, v(k))
        return cls
    return decorate


@check_attributes(name=SizedString(size=8), shares=UnsignedInteger, price=UnsignedFloat)
class Stock2:
    def __init__(self, name, shares, price):
        self.name = name
        self.shares = shares
        self.price = price


# 메타클래스 방식
class checkmeta(type):
    def __new__(cls, clsname, bases, methods):
        # 디스크립터에 속성이름 붙이기
        for k, v in methods.items():
            # print(k, v)
            if isinstance(v, Descriptor):
                v.name = k
        return type.__new__(cls, clsname, bases, methods)


class Stock3(metaclass=checkmeta):
    name = SizedString(size=8)
    shares = UnsignedInteger()
    price = UnsignedFloat()

    def __init__(self, name, shares, price):
        self.name = name
        self.shares = shares
        self.price = price


# 일반
class Point:
    x = Integer('x')
    y = Integer('y')


# 메타클래스
class Point2(metaclass=checkmeta):
    x = Integer()
    y = Integer()


print(Point.x.name)
print(Point2.x.name)
print(Point2.y.name)


