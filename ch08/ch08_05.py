"""
8.5 클래스 이름의 캡슐화
문제: 파이썬은 접근 제어 기능이 부족
해결: 이름에 특정 규칙, _ 시작, ex) sys._getframe()
"""


class A:
    def __init__(self):
        self._internal = 0
        self.public = 1
        self.__private = 0

    def public_method(self):
        pass

    def _internal_method(self):
        self.__private_method()
        pass

    def __private_method(self):  # __ 두 개는 오버라이드 불가
        lambda_ = 2  # 키워드와 충돌 시엔 _를 앞이 아닌 뒤에 붙인다.
        pass
