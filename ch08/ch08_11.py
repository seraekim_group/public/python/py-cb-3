"""
8.11 자료 구조 초기화 단순화하기
해결: 베이스 클래스의 __init__() 정의
"""


class Structure:
    # 예상되는 필드를 명시하는 클래스 변수
    _fields = []

    def __init__(self, *args, **kwargs):
        if len(args) + len(kwargs) != len(self._fields):
            raise TypeError('Expected {} arguments'.format(len(self._fields)))

        # 위치 매개변수 설정
        for name, value in zip(self._fields, args):
            setattr(self, name, value)

        # 키워드 매개변수 설정
        for name in self._fields[len(args):]:
            setattr(self, name, kwargs.pop(name))

        # 남아 있는 기타 매개변수 확인
        if kwargs:
            raise TypeError('Invalid argument(s): {}'.format(','.join(kwargs)))


# 없는 필드가 있는 경우 동적으로 추가
class Structure2:
    _fields = []

    def __init__(self, *args, **kwargs):
        if len(args) != len(self._fields):
            raise TypeError('Expected {} arguments'.format(len(self._fields)))

        for name, value in zip(self._fields, args):
            setattr(self, name, value)

        # 추가
        extra_args = kwargs.keys() - self._fields
        for name in extra_args:
            setattr(self, name, kwargs.pop(name))

        if kwargs:
            raise TypeError('Invalid argument(s): {}'.format(','.join(kwargs)))


if __name__ == '__main__':
    import math

    class Stock(Structure):
        _fields = ['name', 'shares', 'price']

    class Stock2(Structure2):
        _fields = ['name', 'shares', 'price']

    class Point(Structure):
        _fields = ['x', 'y']

    class Circle(Structure):
        _fields = ['radius']
        def area(self):
            return math.pi * self.radius ** 2

    s = Stock('ACME', 50, 90.1)
    p = Point(2, 3)
    c = Circle(4.5)
    s2 = Stock('ACME', 50, price=122)
    print(s2.price)
    s3 = Stock2('ACME', 50, 90.1, data='8/12/1989')

# 단점 - 문서화, IDE 도움말이 설명 안됨, 9.16 type signature 참고


def init_fromlocals(self):
    import sys
    locs = sys._getframe(1).f_locals
    for k, v in locs.items():
        if k != 'self':
            setattr(self, k, v)


# frame hack 으로 인스턴스 변수 자동으로 초기화
# 성능이 50% 느려짐
class Stock3:
    def __init__(self, name, shares, price):
        init_fromlocals(self)


# 자동완성 기능 등 다 잘된다.
# missing 2 required positional arguments: 'name' and 'shares'
s4 = Stock3(price=33)
