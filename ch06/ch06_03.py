"""
6.3 단순한 XML 데이터 파싱
해결: xml.etree.ElementTree
"""

from urllib.request import urlopen
from xml.etree.ElementTree import parse
from lxml.etree import parse

u = urlopen('https://planetpython.org/rss20.xml')
doc = parse(u)

# 관심 있는 태그 출력
for item in doc.iterfind('channel/item'):  # channel 요소의 item 요소
    title = item.findtext('title')  # 상대적 위치
    date = item.findtext('pubDate')
    link = item.findtext('link')

    print(title, date, link, sep='||')

e = doc.find('channel/item')
print(e)
print(e.tag)
print(e.text)