"""
6.6 XML 파싱, 수정, 저장
해결: xml.etree.ElementTree
"""

from xml.etree.ElementTree import parse, Element
doc = parse('ch06_06.xml')
root = doc.getroot()

# 요소 몇 개 제거하기
root.remove(root.find('sri'))
root.remove(root.find('cr'))

# nm 뒤에 삽입
print(root.getchildren().index(root.find('nm')))
e = Element('spam')
e.text = 'This is a test'
root.insert(2, e)

# 파일에 쓰기
doc.write('ch06_06_2.xml', xml_declaration=True)

