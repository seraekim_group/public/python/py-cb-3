"""
6.1 CSV 데이터 읽기와 쓰기
해결: csv
"""

import csv
from collections import namedtuple

with open('ch06_01_stocks.csv', encoding='UTF-8') as f:
    # 1, 2
    # f_csv = csv.reader(f)
    # headers = next(f_csv)
    # print(headers)

    # 1
    # for row in f_csv:
    #     print(row)

    # 2
    # Row = namedtuple('Row', headers)
    # for r in f_csv:
    #     row = Row(*r)
    #     print(row)

    # 3
    f_csv = csv.DictReader(f)
    for row in f_csv:
        print(row)


# print(row[0])
# print(row.Price)
print(row['Change'])

headers = ['Symbol','Price','Date','Time','Change','Volume']
rows = [['AA', 30.48, '6/11/2007', '9:36am', -0.18, 181800],
        ['AA2', 30.48, '6/11/2007', '9:36am', -0.18, 181800]]

with open('ch06_01_stocks2.csv', 'w', newline='') as f:
    f_csv = csv.writer(f)
    f_csv.writerow(headers)
    f_csv.writerows(rows)

# 탭으로 구분한 값 읽기
print('탭으로 구분한 값 읽기')
with open('ch06_01_stocks.csv', encoding='utf-8') as f:
    f_tsv = csv.reader(f, delimiter='\t')
    for row in f_tsv:
        print(row)

# tuple ValueError 예외 처리
print('tuple ValueError 예외 처리')
import re
with open('ch06_01_stocks3.csv') as f:
    f_csv = csv.reader(f)
    headers = [re.sub('[^a-zA-Z_]', '_', h) for h in next(f_csv)]
    Row = namedtuple('Row', headers)
    for r in f_csv:
        row = Row(*r)
        print(row)

# 타입 변환
print('타입 변환')
col_types = [str, float, str, str, float, int]
with open('ch06_01_stocks.csv', encoding='utf-8') as f:
    f_csv = csv.reader(f)
    headers = next(f_csv)
    for row in f_csv:
        row = tuple(convert(value) for convert, value in zip(col_types, row))
        print(row)

# 선택한 필드만 변환하는 예제
print('선택한 필드만 변환하는 예제')
field_types = [('Price', float), ('Change', float), ('Volume', int)]

with open('ch06_01_stocks.csv', encoding='utf-8') as f:
    for row in csv.DictReader(f):
        row.update((key, conversion(row[key])) for key, conversion in field_types)
        print(row)
