"""
6.2 JSON 데이터 읽기 쓰기
해결: json
"""

import json

data = {
    'name': 'ACME',
    'shares': 100,
    'price': 542.23
}

# json 으로
json_str = json.dumps(data)

# dict 로
data = json.loads(json_str)

# 파일의 경우 dump, load 로

# True -> true, False -> false, None -> null

# pprint는 알파벳순 나열, 딕셔너리를 보기 좋게 출력

# from urllib.request import urlopen
# # u = urlopen('http://search.twitter.com/search.json?q=python&rpp=5')
# # u = urlopen('https://api.twitter.com/1.1/search/tweets.json?q=python&count=5')
# u = urlopen('https://twitter.com/i/search/timeline?f=realtime&q=python')
# resp = json.loads(u.read().decode('utf-8'))
# from pprint import pprint
# pprint(resp)

# 순서지키면서 디코딩하기
s = '{"name": "ACME", "shares": 50, "price": 490.1}'
from collections import OrderedDict
data = json.loads(s, object_pairs_hook=OrderedDict)
print(data)


class JSONObject:
    def __init__(self, d):
        self.__dict__ = d


data = json.loads(s, object_hook=JSONObject)
print(data)
print(data.name)

data = json.loads(s)
# 들여쓰기
print(json.dumps(data, indent=4))
# 정렬
print(json.dumps(data, sort_keys=True))

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y


# 인스턴스 직렬화
def serialize_instance(obj):
    d = {'__classname__': type(obj).__name__}
    d.update(vars(obj))
    return d


# 알려지지 않은 클래스에 이름을 매핑한 dict
classes = {
    'Point': Point
}


def unserialize_object(d):
    clsname = d.pop('__classname__', None)
    if clsname:
        cls = classes[clsname]
        obj = cls.__new__(cls)  # __init__ 호출없이 인스턴스 생성
        for key, value in d.items():
            setattr(obj, key, value)
            return obj
    else:
        return d


p = Point(2, 3)
s = json.dumps(p, default=serialize_instance)
print(s)
a = json.loads(s, object_hook=unserialize_object)
print(a)
print(a.x)
