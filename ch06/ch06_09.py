"""
6.9 16진수 인코딩, 디코딩
해결: binascii, base64
"""

import binascii
import base64

s = b'hello'
h = binascii.b2a_hex(s)
print(h)

print(binascii.a2b_hex(h))

# 대소문자 구분
h = base64.b16encode(s)
print(h)
print(base64.b16decode(h))

# 유니코드는 디코딩 과정이 추가
print(h.decode('ascii'))
