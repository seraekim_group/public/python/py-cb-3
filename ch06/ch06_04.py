"""
6.4 매우 큰 XML 파일 증분 파싱하기
해결: 증분 데이터 처리엔 언제나 iterator 와 generator 를 떠올려야 한다.
"""

from xml.etree.ElementTree import iterparse, parse
from collections import Counter

def parse_and_remove(filename, path):
    path_parts = path.split('/')
    doc = iterparse(filename, ('start', 'end'))
    # root elem 건너 뛰기
    next(doc)

    tag_stack = []
    elem_stack = []
    for event, elem in doc:
        if event == 'start':
            tag_stack.append(elem.tag)
            elem_stack.append(elem)
        elif event == 'end':
            if tag_stack == path_parts:
                yield elem
                elem_stack[-2].remove(elem)  # 메모리를 줄여줌, 요소를 부모로부터 제거, 참조 사라짐. 단, 속도는 2배정도 줄어듬
            try:
                tag_stack.pop()
                elem_stack.pop()
            except IndexError:
                pass


potholes_by_zip = Counter()
# doc = parse('ch06_04.xml')
# for pothole in doc.iterfind('row/row'):
#     potholes_by_zip[pothole.findtext('zip')] += 1
#
# for zipcode, num in potholes_by_zip.most_common():
#     print(zipcode, num)

# 메모리가 수십배 줄어듬
data = parse_and_remove('ch06_04.xml', 'row/row')
for pothole in data:
    potholes_by_zip[pothole.findtext('zip')] += 1

for zipcode, num in potholes_by_zip.most_common():
    print(zipcode, num)

data = iterparse('ch06_04.xml', ('start', 'end'))
print(next(data))
print(next(data))
print(next(data))
print(next(data))
