"""
6.7 네임스페이스로 XML 문서 파싱
해결
"""

from xml.etree.ElementTree import parse, iterparse


class XMLNamespaces:
    def __init__(self, **kwargs):
        self.namespaces = {}
        for name, uri in kwargs.items():
            self.register(name, uri)

    def register(self, name, uri):
        self.namespaces[name] = '{'+uri+'}'

    def __call__(self, path):
        return path.format_map(self.namespaces)


doc = parse('ch06_07.xml')
ns = XMLNamespaces(html='http://www.w3.org/1999/xhtml')
print(doc.find(ns('content/{html}html')))
print(doc.findtext(ns('content/{html}html/{html}head/{html}title')))

for evt, elem in iterparse('ch06_07.xml', ('end', 'start-ns', 'end-ns')):
    print(evt, elem)

# 고급 기능은 lxml에.. DTD검증, XPath 등
