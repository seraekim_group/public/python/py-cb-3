"""
6.11 바이너리 배열 구조체 읽고 쓰기
해결: struct
"""

from struct import Struct
from collections import namedtuple
import numpy as np


def write_records(records, format, f):
    """일련의 튜플을 구조체의 바이너리 파일에 기록"""
    record_struct = Struct(format)
    for r in records:
        f.write(record_struct.pack(*r))


# 파일을 튜플 리스트로 읽기
# 조각 조각 읽어 들이기
def read_records(format, f):
    record_struct = Struct(format)
    # iter, 5.8절 참고
    chunks = iter(lambda: f.read(record_struct.size), b'')
    return (record_struct.unpack(chunk) for chunk in chunks)


# iter를 쓰지 않는다면...
def read_records2(format, f):
    record_struct = Struct(format)
    while True:
        chk = f.read(record_struct.size)
        if chk == b'':
            break
        yield record_struct.unpack(chk)

# 한 번에 읽어들이고 추후 조각 변환
def unpack_records(format, data):
    record_struct = Struct(format)
    # unpack_from 방대한 배열에서 추출 시 유리, 메모리 복사 없음.
    # unpack_from 대신 unpack을 사용한다면, 조각을 만들고 오프셋 계산을 해야 함.
    return (record_struct.unpack_from(data, offset) for offset in range(0, len(data), record_struct.size))


# unpack을 쓰는 경우, 데이터 복사, 객체조각 생성 등...
def unpack_records2(format, data):
    record_struct = Struct(format)
    return (record_struct.unpack(data[offset:offset + record_struct.size])
            for offset in range(0, len(data), record_struct.size))


if __name__ == '__main__':
    records = [(1, 2.3, 4.5), (6, 7.8, 9.0), (12, 13.4, 56.7)]

    with open('ch06_11.bin', 'wb') as f:
        # little endian 32 bit integer, 소수점 두 자리 정확도 부동 소수점
        # i/d/f 32비트정수/64비트부동소수/32비트부동소수
        # <는 바이트 순서, little endian, > big endian, ! network byte order
        write_records(records, '<idd', f)

    with open('ch06_11.bin', 'rb') as f:
        for rec in read_records('<idd', f):
            print(rec)

    with open('ch06_11.bin', 'rb') as f:
        data = f.read()
        for rec in unpack_records('<idd', data):
            print(rec)

    record_struct = Struct('<idd')
    print(record_struct.size)
    _ = record_struct.pack(1, 2.0, 3.0)
    print(_)
    print(record_struct.unpack(_))

    Record = namedtuple('Record', ['kind', 'x', 'y'])

    with open('ch06_11.bin', 'rb') as f:
        records = (Record(*r) for r in read_records('<idd', f))
        for r in records:
            print(r.kind, r.x, r.y)

    # numpy 활용
    with open('ch06_11.bin', 'rb') as f:
        records = np.fromfile(f, dtype='<i,<d,<d')
        print(records)
        print(records[0])

