"""
6.10 Base64 인코딩, 디코딩
해결: base64
"""

import base64
s = b'hello'
a = base64.b64encode(s)
print(a)
print(base64.b64decode(a))
print(base64.b64encode(s).decode('ascii'))

