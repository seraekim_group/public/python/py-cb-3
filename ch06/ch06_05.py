"""
6.5 딕셔너리를 XML로 바꾸기
해결: xml.etree.ElementTree
"""

from xml.etree.ElementTree import Element
from xml.etree.ElementTree import tostring
from xml.sax.saxutils import escape, unescape


def dict_to_xml(tag, d):
    elem = Element(tag)
    for key, val in d.items():
        child = Element(key)
        child.text = str(val)
        elem.append(child)
    return elem


s = {'name': 'GOOG', 'shares': 100, 'price': 490.1}
e = dict_to_xml('stock', s)
print(e)
print(tostring(e))


def dict_to_xml_str(tag, d):
    parts = ['<{}>'.format(tag)]
    for key, val in d.items():
        parts.append('<{0}>{1}</{0}>'.format(key, escape(val)))
    parts.append('</{}>'.format(tag))
    return ''.join(parts)


d = {'name': '<spam>'}
print(tostring(dict_to_xml('hell', d)))
print(dict_to_xml_str('hell', d))


