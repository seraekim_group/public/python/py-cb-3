"""
6.8 관계형 데이터베이스 작업
해결: 튜플
"""

stocks = [
    ('GOOG', 100, 490.1),
    ('AAPL', 50, 490.1),
    ('FB', 150, 490.1),
    ('HPQ', 75, 490.1),
]

import sqlite3
db = sqlite3.connect('database.db')

c = db.cursor()
# c.execute('create table portfolio (symbol text, shares integer, price real)')
# db.commit()

c.execute('delete from portfolio')
db.commit()

c.executemany('insert into portfolio values (?, ?, ?)', stocks)
db.commit()

for row in db.execute('select * from portfolio'):
    print(row)

min_price = 100
for row in db.execute('select * from portfolio where price >= ?', (min_price,)):
    print(row)

# SQLAlchemy ORM
