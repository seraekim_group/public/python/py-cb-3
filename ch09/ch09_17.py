"""
9.17 클래스 코딩 규칙 강제
해결: type상속, __new__(), __init__()
"""


class MyMeta(type):
    """
    clsname: 정의하려는 클래스명
    bases: 베이스 클래스의 튜플
    clsdict: 클래스 딕셔너리
    """
    def __new__(mcs, clsname, bases, clsdict):
        return super().__new__(mcs, clsname, bases, clsdict)


class MyMeta2(type):
    def __init__(self, clsname, bases, clsdict):
        super().__init__(clsname, bases, clsdict)


# 클래스를 정의할 때 대소문자를 혼용할 수 없도록 하는 예제
class NoMixedCaseMeta(type):
    def __new__(mcs, clsname, bases, clsdict):
        for name in clsdict:
            if name.lower() != name:
                raise TypeError('Bad attribute name: ' + name)
        return super().__new__(mcs, clsname, bases, clsdict)


class Root(metaclass=NoMixedCaseMeta):
    pass


class A(Root):
    def foo_bar(self):  # 허용
        pass


#class B(Root):
#    def fooBar(self):  # 에러
#        pass


# 재정의한 메소드의 정의부를 보고 슈퍼클래스의 메소드와 동일한 호출 시그니처를 가지는지 확인
from inspect import signature
import logging


class MatchSignaturesMeta(type):
    def __init__(self, clsname, bases, clsdict):
        super().__init__(clsname, bases, clsdict)
        sup = super(self, self)
        for name, value in clsdict.items():
            if name.startswith('_') or not callable(value):
                continue

            # 있다면 앞의 정의를 가져와서 시그니처를 비교
            prev_dfn = getattr(sup, name, None)
            if prev_dfn:
                prev_sig = signature(prev_dfn)
                val_sig = signature(value)
                if prev_sig != val_sig:
                    logging.warning('Signature mismatch in %s. %s != %s', value.__qualname__, prev_sig, val_sig)


class Root2(metaclass=MatchSignaturesMeta):
    pass


class A2(Root2):
    def foo(self, x, y):
        pass

    def spam(self, x, *, z):
        pass


class B2(A2):
    def foo(self, a, b):
        pass

    def spam(self, x, y):
        pass


