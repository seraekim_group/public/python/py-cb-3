"""
9.4 매개변수를 받는 데코레이터 정의
"""

from functools import wraps
import logging

logging.basicConfig(level=logging.DEBUG)


def logged(level, name=None, message=None):
    """함수에 로깅 추가"""
    def decorate(func):
        logname = name if name else func.__module__
        log = logging.getLogger(logname)
        logmsg = message if message else func.__name__

        @wraps(func)
        def wrapper(*args, **kwargs):
            log.log(level, logmsg)
            return func(*args, **kwargs)
        return wrapper
    return decorate


@logged(logging.INFO, 'hel', 'xxxx')
def add(x, y):
    return x + y


@logged(logging.CRITICAL, 'example')
def spam():
    print('Spam!')


add(1, 5)
spam()

