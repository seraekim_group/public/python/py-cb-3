"""
9.10 클래스와 스태틱 메소드에 데코레이터 적용
해결: @ 순서가 중요.
"""

import time
from functools import wraps


def timethis(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        start = time.time()
        r = func(*args, **kwargs)
        end = time.time()
        print(end - start)
        return r
    return wrapper


class Spam:
    @timethis
    def instance_method(self, n):
        print(self, n)
        while n > 0:
            n -= 1

    @classmethod
    @timethis
    def class_method(cls, n):
        print(cls, n)
        while n > 0:
            n -= 1

    @staticmethod
    @timethis
    def static_method(n):
        print(n)
        while n > 0:
            n -= 1


s = Spam()
s.instance_method(1000000)
s.class_method(1000000)
s.static_method(1000000)

# @method , @timethis 순서가 바뀌면 크래시 발생, @method는 디스크립터 객체를 만든다.
# 이와 같은 상황으로 8.12절, @classmehtod와 @abstractmethod 순서를 지켜야 한다.