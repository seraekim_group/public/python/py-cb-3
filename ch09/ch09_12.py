"""
9.12 클래스 정의 패치에 데코레이터 사용
해결: __getattribute__
"""


def log_getattribute(cls):
    # 원본 구현 얻기
    orig_getattribute = cls.__getattribute__

    # 새로운 정의 생성
    def new_getattribute(self, name):
        print('getting:', name)
        return orig_getattribute(self, name)

    # 클래스에 붙이고 반환
    cls.__getattribute__ = new_getattribute
    return cls


@log_getattribute
class A:
    def __init__(self, x):
        self.x = x

    def spam(self):
        pass


a = A(42)
print(a.x)
a.spam()


# 상속을 활용하는 경우, super 활용으로 데코레이터 보단 느려진다.
class LoggedGetattribute:
    def __getattribute__(self, item):
        print('getting:', item)
        return super().__getattribute__(item)


class A2(LoggedGetattribute):
    def __init__(self, x):
        self.x = x

    def spam(self):
        pass

