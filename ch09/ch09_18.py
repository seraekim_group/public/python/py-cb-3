"""
9.18 클래스를 프로그램적으로 정의
문제: 코드를 문자열로 만들고 exec() 같은 함수로 할까 생각 중
해결: types.new_class()
"""

# 이상함 안되는 코드..........

def __init__(self, name, shares, price):
     self.name = name
     self.shares = shares
     self.price = price


def cost(self):
    return self.shares * self.price


cls_dict = {
    '__init__': __init__,
    'cost': cost,
}

import types
import abc

Stock = types.new_class('Stock', (), dict(metaClass=abc.ABCMeta), lambda ns: ns.update(cls_dict))
Stock.__module__ = __name__

s = Stock('ACME', 50, 91.1)
print(Stock)
print(type(Stock))
print(s)
print(s.cost())

# 세 번째 인자에 메타클래스 말고 키워드 매개변수도 넣을 수 있다.
# class Spam(Base, debug=True, typecheck=False): ...
Spam = types.new_class('Spam', (Stock,), {'debug': True, 'typecheck': False}, lambda ns: ns.update(cls_dict))

import collections
# exec 를 쓰는 경우가 있다.
Stock = collections.namedtuple('Stock', ['name', 'shares', 'price'])
print(Stock)

# 직접 만드는 예제
import operator
import sys


def named_tuple(classname, fieldnames):
    cls_dict = {
        name: property(operator.itemgetter(n)) for n, name in enumerate(fieldnames)
    }

    def __new__(cls, *args):
        if len(args) != len(fieldnames):
            raise TypeError('Expected {} arguments'.format(len(fieldnames)))
        return tuple.__new__(cls, args)

    cls_dict['__new__'] = __new__

    # 클래스 만들기
    cls = types.new_class(classname, (tuple,), {}, lambda ns: ns.update(cls_dict))

    # 호출자에 모듈 설정 frame hack
    cls.__module__ = sys._getframe(1).f_globals['__name__']
    return cls


Point = named_tuple('Point', ['x', 'y'])
print(Point)
p = Point(4, 5)
print(len(p))
print(p.x)
print(p.y)
print('%s %s' % p)
# p.x = 2 # AttributeError: can't set attribute

