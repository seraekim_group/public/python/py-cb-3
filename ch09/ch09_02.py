"""
9.2 데코레이터 작성 시 함수 메타데이터 보존
해결: functools.wraps
"""

import time
from functools import wraps


def timethis(func):
    @wraps(func)  # 메타데이터 복사하기
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print(func.__name__, end-start)
        return result
    return wrapper


@timethis
def countdown(n: int):
    print('원본함수')
    while n > 0:
        n -= 1


countdown(10000)
print(countdown.__name__)
print(countdown.__doc__)
print(countdown.__annotations__)

# 감싼 함수에 직접적으로 접근하려면?
countdown.__wrapped__(10000)


