"""
9.1 함수 감싸기
해결: 데코레이터 함수 정의
"""

import time
from functools import wraps


def timethis(func):
    """실행 시간을 보고하는 데코레이터"""

    @wraps(func)
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print(func.__name__, end-start)
        return result
    return wrapper


# @timethis
def countdown(n):
    while n > 0:
        n -= 1


countdown(10000)

countdown2 = timethis(countdown)
countdown2(10000)


class A:
    @classmethod
    def method(cls):
        pass


class B:
    def method(cls):
        pass
    method = classmethod(method)

