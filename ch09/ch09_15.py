"""
9.15 옵션 매개변수를 받는 메타클래스 정의
해결: __prepare__(), __new__(), __init__()
"""

from abc import ABCMeta, abstractmethod


class MyMeta(type):
    # 옵션
    @classmethod
    def __prepare__(mcs, name, bases, *, debug=False, synchronize=False):
        # 커스텀 처리...
        return super().__prepare__(name, bases)

    # 필수
    def __new__(cls, name, bases, ns, *, debug=False, synchronize=False):
        # 커스텀 처리...
        return super().__new__(cls, name, bases, ns)

    # 필수
    def __init__(self, name, bases, ns, *, debug=False, synchronize=False):
        # 커스텀 처리...
        super().__init__(name, bases, ns)


class Spam(metaclass=MyMeta):
    debug = True
    synchronize = True


