"""
9.7 데코레이터를 사용해서 함수에서 타입 확인 강제
"""

from inspect import signature
from functools import wraps


def typeassert(*ty_args, **ty_kwargs):
    def decorate(func):
        # 디버그 모드일 때만..
        print(__debug__)
        if not __debug__:
            return func

        sig = signature(func)
        bound_types = sig.bind_partial(*ty_args, **ty_kwargs).arguments

        @wraps(func)
        def wrapper(*args, **kwargs):
            bound_values = sig.bind(*args, **kwargs)
            for name, value in bound_values.arguments.items():  # 정렬된 딕셔너리 생성, 시그니처와 동일한 순서
                if name in bound_types:
                    if not isinstance(value, bound_types[name]):
                        raise TypeError('Argument {} must be {}'.format(name, bound_types[name]))
            return func(*args, **kwargs)
        return wrapper
    return decorate


@typeassert(int, z=int)
def spam(x, y, z='None'):  # 기본값은 assert가 적용안됨, 42를 다른값으로 바꿔도 상관없음.
    print(x, y, z)

spam(1, 2)
spam(1, 2, 3)
spam(1, 'hel', 3)
spam(1, 'hel', 'wolr')
