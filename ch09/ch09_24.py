"""
9.24 파이썬 코드 파싱, 분석
해결: exec 외에도 ast
"""

import ast
ex = ast.parse('2 + 3*4 + x', mode='eval')
print(ast.dump(ex))


class CodeAnalyzer(ast.NodeVisitor):
    def __init__(self):
        self.loaded = set()
        self.stored = set()
        self.deleted = set()

    def visit_Name(self, node):
        if isinstance(node.ctx, ast.Load):
            self.loaded.add(node.id)
        elif isinstance(node.ctx, ast.Store):
            self.stored.add(node.id)
        elif isinstance(node.ctx, ast.Del):
            self.deleted.add(node.id)


if __name__ == '__main__':
    code = '''
for i in range(10):
    print(i)
del i
'''

    top = ast.parse(code, mode='exec')

    c = CodeAnalyzer()
    c.visit(top)
    print(c.loaded)
    print(c.stored)
    print(c.deleted)

exec(compile(top, '<stdin>', 'exec'))
