"""
9.23 지역변수 문제 없이 코드 실행
"""

# 전역 네임스페이스에서
#a = 13
# b = 0
#exec('b = a + 1')
#print(b)


# 함수 내부에서
def test():
    a = 13
    b = 0
    exec('b = a + 1')
    print(b)


test()


# 지역변수를 얻어야 함
def test2():
    a = 13
    loc = locals()
    exec('b = a + 1')
    b = loc['b']
    print(b)


# 직접 넣는 방식
def test3():
    a = 13
    loc = {'a': a}
    exec('b = a + 1', {}, loc)
    b = loc['b']
    print(loc)


test3()

