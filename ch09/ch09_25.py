"""
9.25 파이썬 바이트 코드 디스어셈블
문제: low level, bytecode, disassemble
해결: dis
"""


def countdown(n):
    while n > 0:
        print('T-minus', n)
        n -= 1
    print('Blastoff!')


import dis

dis.dis(countdown)

c = countdown.__code__.co_code
print(c)

import opcode

print(opcode.opname[c[0]])
print(opcode.opname[c[3]])


# low bytecode sequence to opcode generator
def generate_opcodes(codebytes):
    extended_arg = 0
    i = 0
    n = len(codebytes)
    while i < n:
        op = codebytes[i]
        i += 1
        if op >= opcode.HAVE_ARGUMENT:
            oparg = codebytes[i] + codebytes[i+1]*256 + extended_arg
            extended_arg = 0
            i += 2
            if op == opcode.EXTENDED_ARG:
                extended_arg = oparg * 65536
                continue
        else:
            oparg = None
        yield (op, oparg)


for op, oparg in generate_opcodes(countdown.__code__.co_code):
    print(op, opcode.opname[op], oparg)
