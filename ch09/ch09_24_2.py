import ast
import inspect


# 전역적으로 접근하는 이름을 함수 내부의 지역변수로
# 바꿔주는 노드 비지터
class NameLower(ast.NodeVisitor):
    def __init__(self, lowered_names):
        self.lowered_names = lowered_names

    def visit_FunctionDef(self, node):
        code = '__globals = globals()\n'
        code += '\n'.join("{0} = __globals['{0}']".format(name)
                          for name in self.lowered_names)
        code_ast = ast.parse(code, mode='exec')

        # 함수 내부에 새로운 구문을 추가
        node.body[:0] = code_ast.body

        # 함수 객체 저장
        self.func = node


# 전역 이름을 지역으로 바꾸는 데코레이터
def lower_names(*namelist):
    def lower(func):
        srclines = inspect.getsource(func).splitlines()
        # @lower_names 데코레이터 이전의 소스 생략
        for n, line in enumerate(srclines):
            if '@lower_names' in line:
                break

        src = '\n'.join(srclines[n+1:])
        # 들여쓴 코드를 처리하기 위한 트릭
        if src.startswith((' ', '\t')):
            src = 'if 1:\n' + src
        top = ast.parse(src, mode='exec')

        # AST 변형
        cl = NameLower(namelist)
        cl.visit(top)

        # 수정한 AST 실행
        temp = {}
        exec(compile(top, '', 'exec'), temp, temp)

        # 수정한 코드 객체 추출
        func.__code__ = temp[func.__name__].__code__
        return func
    return lower


INCR = 1


# 20% 더 빨라짐
@lower_names('INCR')
def countdown(n):
    while n > 0:
        n -= INCR


# 다음과 같이 변형
def countdown2(n):
    __globals = globals()
    INCR = __globals['INCR']
    while n > 0:
        n -= INCR


countdown(100)
countdown2(100)
