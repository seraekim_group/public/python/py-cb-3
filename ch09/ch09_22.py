"""
9.22 손쉬운 콘텍스트 매니저 정의
문제: with 와 사용할 context manager 구현
해결: contextlib.contextmanager 데코레이터
"""

import time
from contextlib import contextmanager


# yield 이전은 context manager의 __enter__(), 이후는 __exit__() 처럼 실행
@contextmanager
def timethis(label):
    start = time.time()
    try:
        yield
    finally:
        end = time.time()
        print('{}: {}'.format(label, end - start))


with timethis('counting'):
    n = 10000000
    while n > 0:
        n -= 1


@contextmanager
def list_transaction(orig_list):
    working = list(orig_list)
    yield working
    orig_list[:] = working


items = [1, 2, 3]
with list_transaction(items) as working:
    working.append(4)
    working.append(5)


print(items)

# 예외가 발생하지 않은 경우에만 리스트 수정사항이 반영
with list_transaction(items) as working:
    working.append(4)
    working.append(5)
    #raise RuntimeError('oops')

# 일반적으론 enter, exit 정의
class timethis2:
    def __init__(self, label):
        self.lable = label
    def __enter__(self):
        self.start = time.time()
    def __exit__(self, exc_type, exc_val, exc_tb):
        end = time.time()
        print('{}: {}'.format(self.label, end - self.start))

