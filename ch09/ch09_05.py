"""
9.5 사용자가 조절 가능한 속성을 가진 데코레이터 정의
해결: nonlocal accessor function
"""

from functools import wraps, partial
import logging


def attach_wrapper(obj, func=None):
    if func is None:
        return partial(attach_wrapper, obj)
    setattr(obj, func.__name__, func)
    return func


def logged(level, name=None, message=None):
    """
    name, message 기본값은 함수의 모듈 이름
    """
    def decorate(func):
        logname = name if name else func.__module__
        log = logging.getLogger(logname)
        logmsg = message if message else func.__name__

        @wraps(func)
        def wrapper(*args, **kwargs):
            log.log(level, logmsg)
            return func(*args, **kwargs)

        @attach_wrapper(wrapper)
        def set_level(newlevel):
            nonlocal level
            level = newlevel

        @attach_wrapper(wrapper)
        def set_message(newmsg):
            nonlocal logmsg
            logmsg = newmsg

        @attach_wrapper(wrapper)
        def get_level():
            return level

        # 대안
        wrapper.get_level = lambda: level
        return wrapper
    return decorate

# 사용 예
@logged(logging.DEBUG)
def add(x, y):
    return x + y


@logged(logging.CRITICAL, 'example')
def spam():
    print('Spam!')

logging.basicConfig(level=logging.DEBUG)
add(2, 3)
add.set_message('Add called')
add(2, 3)
add.set_level(logging.WARNING)
add(2, 3)

from ch09.ch09_02 import timethis


@timethis
@logged(logging.DEBUG)
def countdown(n):
    while n > 0:
        n -= 1


countdown(10000000)
