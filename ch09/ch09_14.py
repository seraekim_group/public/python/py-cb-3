"""
9.14 클래스 속성 정의 순서 수집
해결: OrderedDict
"""

from collections import OrderedDict


class Typed:
    _expected_type = type(None)

    def __init__(self, name=None):
        self.name = name

    def __set__(self, instance, value):
        if not isinstance(value, self._expected_type):
            raise TypeError('Expected ' + str(self._expected_type))
        instance.__dict__[self._name] = value


class Integer(Typed):
    _expected_type = int


class Float(Typed):
    _expected_type = float


class String(Typed):
    _expected_type = str


class OrderedMeta(type):
    def __new__(cls, clsname, bases, clsdict):
        d = dict(clsdict)
        order = []
        for name, value in clsdict.items():
            if isinstance(value, Typed):
                value._name = name
                order.append(name)
        d['_order'] = order
        return type.__new__(cls, clsname, bases, d)

    # 클래스 몸체를 처리할 때 사용할 매핑 객체 반환
    @classmethod
    def __prepare__(mcs, name, bases):
        return OrderedDict()


# 인스턴스 데이터를 csv로 직렬화
class Structure(metaclass=OrderedMeta):
    def as_csv(self):
        return '.'.join(str(getattr(self, name)) for name in self._order)


class Stock(Structure):
    name = String()
    shares = Integer()
    price = Float()

    def __init__(self, name, shares, price):
        self.name = name
        self.shares = shares
        self.price = price


s = Stock('GOOG', 100, 490.1)
print(s.as_csv())


# 중복 정의를 거부하는 구현
class NoDupOrderedDict(OrderedDict):
    def __init__(self, clsname):
        self.clsname = clsname
        super().__init__()

    def __setitem__(self, key, value):
        if key in self:
            raise TypeError('{} already defined in {}'.format(key, self.clsname))


class OrderedMeta2(type):
    def __new__(cls, clsname, bases, clsdict):
        d = dict(clsdict)  # 마지막 클래스 객체 만들 때 올바른 dict 인스턴스로 변환
        d['_order'] = [name for name in clsdict if name[0] != '_']
        return type.__new__(cls, clsname, bases, d)

    @classmethod
    def __prepare__(mcs, name, bases):
        return NoDupOrderedDict(name)


class A(metaclass=OrderedMeta2):
    def spam(self):
        pass
    def spam(self, test):
        pass


