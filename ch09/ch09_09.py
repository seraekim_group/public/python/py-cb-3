"""
9.9 클래스 데코레이터 정의
문제: 함수를 데코레이터로 감싸지만, 결과는 호출가능 인스턴스가 되도록 하고 싶다.
해결: __call__(), __get__()
"""

import types
from functools import wraps


class Profiled:
    def __init__(self, func):
        # 메타데이터 복사
        wraps(func)(self)
        self.ncalls = 0

    def __call__(self, *args, **kwargs):
        self.ncalls += 1
        return self.__wrapped__(*args, **kwargs)

    def __get__(self, instance, cls):
        if instance is None:
            # 클래스에서 접근 시 Profiled 인스턴스 반환하여 ncalls 접근 가능
            return self
        else:
            # 바운드 메소드 수동 생성
            return types.MethodType(self, instance)


@Profiled
def add(x, y):
    return x + y


class Spam:
    @Profiled
    def bar(self, x):
        print(self, x)


add(2, 3)
print(add.ncalls)
s = Spam()
s.bar(1)
s.bar(1)
s.bar(1)
print(Spam.bar.ncalls)


# 위 과정이 복잡하다면 9.5절, nonlocal 고려
def profiled(func):
    ncalls = 0
    @wraps(func)
    def wrapper(*args, **kwargs):
        nonlocal ncalls
        ncalls += 1
        return func(*args, **kwargs)
    wrapper.ncalls = lambda: ncalls  # 함수 속성 첨부
    return wrapper


@profiled
def add2(x, y):
    return x + y


add2(2, 3)
print(add2(4, 5))
print(add2.ncalls())
