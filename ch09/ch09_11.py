"""
9.11 감싼 함수에 매개변수를 추가하는 데코레이터 작성
해결: keyword-only argument
"""

from functools import wraps
import inspect


def optional_debug(func):
    # 기존 함수에 이미 debug 변수가 있다면?
    if 'debug' in inspect.getfullargspec(func).args:
        raise TypeError('debug argument already defined')

    @wraps(func)
    def wrapper(*args, debug=False, **kwargs):
        if debug:
            print('Calling', func.__name__)
        return func(*args, **kwargs)

    # 시그니처 올바로 관리하기
    sig = inspect.signature(func)
    parms = list(sig.parameters.values())
    parms.append(inspect.Parameter('debug', inspect.Parameter.KEYWORD_ONLY, default=False))
    wrapper.__signature__ = sig.replace(parameters=parms)
    return wrapper


@optional_debug
def spam(a, b, c):
    print(a, b, c)


# @optional_debug
# def spam2(a, b, c, debug='x'):
#     print(a, b, c)


spam(1, 2, 3)
spam(1, 2, 3, debug=True)

# 시그니처 확인해보기
print(inspect.signature(spam))
