"""
9.8 데코레이터를 클래스의 일부로 정의
"""

from functools import wraps


class A:
    # 인스턴스 메소드
    def decorator1(self, func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            print('Decorator 1')
            return func(*args, **kwargs)
        return wrapper

    # 클래스 메소드
    @classmethod
    def decorator2(self, func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            print('Decorator 2')
            return func(*args, **kwargs)
        return wrapper


a = A()


@a.decorator1
def spam():
    pass


@A.decorator2
def grok():
    pass


# 상속
class B(A):
    @A.decorator2
    def bar(self):
        pass

