"""
9.13 인스턴스 생성 조절에 메타클래스 사용
해결: __call__()
"""


# 다른 누구도 인스턴스를 생성하지 못하도록
class NoInstances(type):
    def __call__(self, *args, **kwargs):
        raise TypeError("Can't instantiate directly")


class Spam(metaclass=NoInstances):
    @staticmethod
    def grok(x):
        print('Spam.grok')


# static method 호출은 가능
Spam.grok(42)
# s = Spam()


# singleton 패턴 구현
class Singleton(type):
    def __init__(self, *args, **kwargs):
        self.__instance = None
        super().__init__(*args, **kwargs)

    def __call__(self, *args, **kwargs):
        if self.__instance is None:
            self.__instance = super().__call__(*args, **kwargs)
            return self.__instance
        else:
            return self.__instance


class Spam2(metaclass=Singleton):
    def __init__(self):
        print('Creating Spam')


a = Spam2()
b = Spam2()
print(a == b)


# cache instance, 8.25절 참고
import weakref


class Cached(type):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__cache = weakref.WeakValueDictionary()

    def __call__(self, *args):
        if args in self.__cache:
            return self.__cache[args]
        else:
            obj = super().__call__(*args)
            self.__cache[args] = obj
            return obj


class Spam3(metaclass=Cached):
    def __init__(self, name):
        print('Creating Spam({!r})'.format(name))
        self.name = name


a = Spam3('A')
b = Spam3('b')
c = Spam3('A')

print(a is b)
print(a is c)
