"""
1.6 딕셔너리의 키를 여러 값에 매핑하기
문제: 딕셔너리의 키를 하나 이상의 값에 매핑하고 싶다 ("multidict")
해결: 여러 값을 리스트나 세트와 같은 컨테이너에 따로 저장해 두어야 한다.
"""

# 순서가 중요한 경우 list
d2 = {
    'a': [1, 2, 3],
    'b': [4, 5]
}

# 순서가 상관없는 경우 set
e = {
    'a': {1, 2, 3},
    'b': {4, 5}
}

# 딕셔너리를 쉽게 만들기 위해서
from collections import defaultdict

d = defaultdict(list)
d['a'].append(1)
d['a'].append(2)
d['b'].append(4)

d = defaultdict(set)
d['a'].add(1)
d['a'].add(2)
d['b'].add(4)

# defaultdict는 없는 값이라도 한 번이라도 접근했다면 키의 엔트리 생성.. 이게 싫다면
d = {}  # 일반 딕셔너리
d.setdefault('a', []).append(1)
d.setdefault('a', []).append(2)
d.setdefault('b', []).append(4)

# 첫 번째 값에 대한 초기화를 하려면..
d = {}
for key, value in d2.items():
    if key not in d:
        d[key] = []  # 초기화
    d[key].append(value)
print(d)

d = defaultdict(list)
for key, value in d2.items():
    d[key].append(value)
print(d)
