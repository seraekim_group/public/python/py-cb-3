"""
1.15 필드에 따라 레코드 묶기
문제: 일련의 딕셔너리나 인스턴스가 있고 특정 필드 값에 기반한 그룹의 데이터를 순환하고 싶다.
해결: itertools.groupby()
"""

rows = [
    {'address': '541ZNCLARK', 'date': '07/01/2012'},
    {'address': '5148NCLARK', 'date': '07/04/2012'},
    {'address': '5800E 58TH', 'date': '07/02/2012'},
    {'address': 'ZIZ2 NCLARK', 'date': '07/03/2012'},
    {'address': '5645 NRAVENSWOOD', 'date': '07/02/2012'},
    {'address': '1060WADDISON', 'date': '07/02/2012'},
    {'address': '4801NBROADWAY', 'date': '07/01/2012'},
    {'address': '1039WCRANVILLE', 'date': '07/04/2012'},
]

from operator import itemgetter
from itertools import groupby

# 원하는 필드로 정렬
rows.sort(key=itemgetter('date'))

# 그룹 내부에서 순환한다.
for date, items in groupby(rows, key=itemgetter('date')):
    print(date)
    for i in items:
        print('    ', i)

# 단순히 날짜에 따라 묶기만 할 거면 1.6절과 같이 defaultdict() 로 multidict 구성
from collections import defaultdict
rows_by_date = defaultdict(list)
for row in rows:
    rows_by_date[row['date']].append(row)

for r in rows_by_date['07/01/2012']:
    print(r)
