"""
1.16 시퀀스 필터링
문제: 시퀀스 내부에 데이터가 있고 특정 조건에 따라 값을 추출하거나 줄이고 싶다.
해결: list comprehension
"""

import math
from itertools import compress

my_list = [1, 4, -5, 10, -7, 2, 3, -1]
print([n for n in my_list if n > 0])
print([n for n in my_list if n < 0])

# 생성자 표현식으로 값 걸러내기
pos = (n for n in my_list if n > 0)
print(pos)
for x in pos:
    print(x)

# 복잡한 필터 조건
values = [1, '2', '-3', '-', '4', 'N/A', '5']


def is_int(val):
    try:
        x = int(val)
        return True
    except ValueError:
        return False


ivals = list(filter(is_int, values))
print(ivals)

print([math.sqrt(n) for n in my_list if n > 0])

# 필터링 조건을 조건 표현식으로
clip_neg = [n if n > 0 else 0 for n in my_list]

# 어떤 시퀀스의 필터링 결과를 다른 시퀀스에 반영
addresses = [
    '5412 N C',
    '5148 N C',
    '5800 E 5',
    '2122 N C',
    '5645 N R',
    '1060 W A',
    '4801 N B',
    '1039 W G'
]
counts = [0, 3, 10, 4, 1, 7, 6, 1]

more5 = [n > 5 for n in counts]
print(more5)
# compress 로 True 에 일치하는 값을 골라내며, filter와 같이 iterator를 반환
print(list(compress(addresses, more5)))
