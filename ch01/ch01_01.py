"""
1.1 시퀀스를 개별 변수로 나누기
문제: N개의 요소를 가진 튜플이나 시퀀스가 있다. 이를 변수 N개로 나누어야 한다.
해결: 간단한 할당문 활용
"""
p = (4, 5)
x, y = p
print(x)
print(y)

data = ['ACME', 50, 91.1, (2012, 12, 21)]
name, shares, price, date = data
print(name)
print(date)

name, shares, price, (year, mon, day) = data
print(year)

# 요소 개수가 일치하지 않으면 다음과 같은 에러 발생
# ValueError: not enough values to unpack (expected 3, got 2)
# x, y, z = p

# unpacking은 튜플, 리스트 등 순환 가능한 모든 객체에 적용
a, b, c, d, e = 'Hello'
print(e)

# 단순히 버릴 변수명 지정 가능
_, shares, price, _ = data
print(shares)
print(price)
