"""
1.5 우선 순위 큐 구현
문제: 주어진 우선 순위에 따라 아이템을 정렬하는 큐를 구현하고 항상 우선 순위가 가장 높은
아이템을 먼저 팝하도록 만들어야 한다.
해결: heapq
"""

import heapq


# heappush / heappop, 첫번째 아이템이 가장 작은 크기를 갖게 됨.
class PriorityQueue:
    def __init__(self):
        self._queue = []
        # 같은 우선순위인 경우 아이템이 삽입된 순서대로 정렬하기 위해 필요
        self._index = 0

    def push(self, item, priority):
        heapq.heappush(self._queue, (-priority, self._index, item))
        self._index += 1

    def pop(self):
        test = heapq.heappop(self._queue)
        print(test)
        return test[-1]


class Item:
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return 'Item({!r})'.format(self.name)


q = PriorityQueue()
q.push(Item('foo'), 1)
q.push(Item('bar'), 5)
q.push(Item('spam'), 4)
q.push(Item('grok'), 1)
print(q.pop())
print(q.pop())
print(q.pop())  # 우선 순위가 같은 경우 삽입 순서와 동일하게 반환
print(q.pop())

# unorderable
# a = Item('foo')
# b = Item('bar')
# print(a < b)

a = (1, 0, Item('foo'))
b = (1, 1, Item('bar'))
print(a < b)

# 스레드 간 통신에서 이 큐를 활용하려면 올바른 lock / signal 활용 필요. 12.3절에서 다룸.
