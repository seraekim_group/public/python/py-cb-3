"""
1.18 시퀀스 요소에 이름 매핑
문제: 리스트나 튜플의 위치로 요소에 접근하는 코드가 있다. 가독성을 위해 이름으로 접근 하도록 수정하고 싶다.
해결: collections.namedtuple()
"""

from collections import namedtuple
Subscriber = namedtuple('Subscribe', ['addr', 'joined'])
sub = Subscriber('jonesy@example.com', '2012-10-19')
print(sub.addr)
print(len(sub))


# 위치 기반, 자료의 구조형에 크게 의존
def compute_cost(records):
    total = 0.0
    for rec in records:
        total += rec[1] * rec[2]
    return total


Stock = namedtuple('Stock', ['name', 'shares', 'price'])


def compute_cost2(records):
    total = 0.0
    for rec in records:
        s = Stock(*rec)
        total += s.shares * s.price
    return total


# 네임드 튜플은 딕셔너리와는 다르게 수정이 불가한데, 정해야한다면 _replace로 새로운 튜플을 만든다.
s = Stock('ACME', 100, 123.45)
s = s._replace(shares=75)
print(s)

# _replace() 메소드로 프로토타입으로 활용
Stock = namedtuple('Stock', ['name', 'shares', 'date', 'time', 'price'])

# 프로토타입
stock_prototype = Stock('', 0, None, None, 0.0)


# 딕셔너리를 Stock으로 변환
def dict_to_stock(s):
    return stock_prototype._replace(**s)


a = {'name': 'ACME', 'shares': 100, 'price': 123.45}
print(dict_to_stock(a))

# 여러 인스턴스 요소를 빈번히 수정해야 하는 자료 구조에서는 __slots__, 8.4절 참고
