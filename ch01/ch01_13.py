"""
1.13 일반 키로 딕셔너리 리스트 정렬
문제: 딕셔너리 리스트가 있고, 하나 혹은 그 이상의 딕셔너리 값으로 이를 정렬하고 싶다.
해결: itemgetter
"""

rows = [
    {'fname': 'Brian', 'lname': 'Jones', 'uid': 1003},
    {'fname': 'David', 'lname': 'Beazley', 'uid': 1002},
    {'fname': 'John', 'lname': 'Cleese', 'uid': 1001},
    {'fname': 'Big', 'lname': 'Jones', 'uid': 1004},
]

# 모든 딕셔너리에 포함된 필드 기준 정렬
from operator import itemgetter

rows_by_fname = sorted(rows, key=itemgetter('fname'))
rows_by_uid = sorted(rows, key=itemgetter('uid'))
# 여러 키 가능
rows_by_lfname = sorted(rows, key=itemgetter('lname', 'fname'))
# lambda 가능
# 이 코드도 잘 동작하지만, itemgetter()가 조금 더 빠르다.
rows_by_lfname2 = sorted(rows, key=lambda r: (r['lname'], r['fname']))
print(rows_by_fname)
print(rows_by_uid)
print(rows_by_lfname)

# min max 메소드에서 key로 itemgetter를 쓸 수 있다.
print(min(rows, key=itemgetter('uid')))
