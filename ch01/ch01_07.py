"""
1.7 딕셔너리 순서 유지
문제: 딕셔너리를 만들고, 순환이나 직렬화할 때 순서를 조절하고 싶다.
해결: OrderDict
"""

from collections import OrderedDict
import json

d = OrderedDict()
d['foo'] = 1
d['bar'] = 2
d['spam'] = 3
d['grok'] = 4

print(json.dumps(d))
for key in d:
    print(key, d[key])

# double linked list 를 쓰기에 일반 딕셔너리에 비해 2배 크다. 추가적인 메모리 소비를 주의
