"""
1.10 순서를 깨지 않고 시퀀스의 중복 없애기
문제: 시퀀스에서 중복된 값을 없애고 싶지만, 아이템의 순서는 유지하고 싶다.
해결: 시퀀스의 값이 hash 가능하다면 set와 generator를 사용해서 쉽게 해결
"""


def dedupe(items):
    seen = set()
    for item in items:
        if item not in seen:
            yield item
            seen.add(item)
    # return seen


a = [1, 5, 2, 1, 9, 1, 5, 10]
print(list(dedupe(a)))


# 해시 불가능한 dict의 경우 중복을 없애려면...
def dedupe2(items, key=None):
    seen = set()
    for item in items:
        # hash 가능 타입으로 변환
        val = item if key is None else key(item)
        if val not in seen:
            yield item
            seen.add(val)


a2 = [{'x': 1, 'y': 2}, {'x': 1, 'y': 3}, {'x': 1, 'y': 2}, {'x': 2, 'y': 4}]
print(list(dedupe2(a2, key=lambda d: (d['x'], d['y']))))
print(list(dedupe2(a2, key=lambda d: d['x'])))

# 중복을 없애려면 세트로
print(set(a))

# 파일을 읽을 때 중복된 라인 무시
with open('ch01_03_deque.txt', 'r') as f:
    for line in dedupe(f):
        print(line, end='')

