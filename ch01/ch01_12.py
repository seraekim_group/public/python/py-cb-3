"""
1.12 시퀀스에 가장 많은 아이템 찾기
문제: 시퀀스에 가장 많이 나타난 아이템을 찾고 싶다.
해결: collections.Counter, most_common()
"""

words = [
    'look', 'into', 'my', 'eyes', 'look', 'into', 'my', 'eyes', 'the',
    'eyes', 'the', 'eyes', 'the', 'eyes', 'not', 'around', 'the', 'my',
    'eyes', 'you\'re', 'under'
]

from collections import Counter
word_counts = Counter(words)
top_three = word_counts.most_common(3)
print(top_three)

print(word_counts['not'])
print(word_counts['eyes'])

# 카운트를 수동으로 증가
morewords = ['why', 'are', 'you', 'not', 'looking', 'in', 'my', 'eyes']
for word in morewords:
    word_counts[word] += 1

print(word_counts['eyes'])

# update()
word_counts.update(morewords)
print(word_counts['eyes'])

# 수식 활용
a = Counter(words)
b = Counter(morewords)
c = a + b
print(c)
c = a - b
print(c)

# 딕셔너리를 직접 활용하기 보단, Counter 사용이 권장 됨
