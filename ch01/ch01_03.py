"""
1.3 마지막 N개 아이템 유지
문제: 순환이나 프로세싱 중 마지막으로 발견한 N개의 아이템을 유지하고 싶다.
해결: collections.deque
"""

from collections import deque


def search(lines, pattern, history=5):
    previous_lines = deque(maxlen=history)
    for line in lines:
        if pattern in line:
            # yield 제너레이터 함수로, 검색과정/검색결과 코드 분리
            yield line, previous_lines
        previous_lines.append(line)


# 시간복잡도 O(1)
q = deque(maxlen=3)
q.append(1)
q.append(2)
q.append(3)
q.append(4)
print(q)

q = deque()
q.append(1)
q.append(2)
q.append(3)
q.appendleft(4)
q.pop()
print(q)

# 파일 사용 예
if __name__ == '__main__':
    with open('ch01_03_deque.txt') as f:
        for line, prevlines in search(f, 'python', 4):
            for pline in prevlines:
                print(pline, end='')
            print(line, end='xx')
            print('-'*20)

