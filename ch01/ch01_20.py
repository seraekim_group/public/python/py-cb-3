"""
1.20 여러 매핑을 단일 매핑으로 합치기
문제: 딕셔너리나 매핑이 여러 개 있고, 자료 검색이나 데이터 확인을 위해서 하나의 매핑으로 합치고 싶다.
해결: collections ChainMap
"""

from collections import ChainMap

a = {'x': 1, 'z': 3}
b = {'y': 2, 'z': 4}
c = ChainMap(a, b)
print(c['x'])
print(c['y'])
print(c['z'])  # a의 z, 중복키가 있으며 첫 번째 매핑 값 사용

# 실제로 하나로 합치는 것은 아니며, 딕셔너리 동작을 재정의
print(len(c))
print(list(c.keys()))
print(list(c.values()))

# 매핑값 변경은 언제나 첫 번째 매핑에 영향을 준다
# 즉 del c['y'] 는 불가
c['z'] = 10
c['w'] = 40
del c['z']
print(a)

# ChainMap은 범위가 있는 값(전역, 지역변수)에 사용하면 유용
values = ChainMap()
values['x'] = 1
# 새로운 매핑 추가
values = values.new_child()
values['x'] = 2
# 새로운 매핑 추가
values = values.new_child()
values['x'] = 3
print(values)
print(values['x'])

# 마지막 매핑 삭제
values = values.parents
print(values['x'])
# 마지막 매핑 삭제
values = values.parents
print(values['x'])

# ChainMap의 대안으로 update()를 사용해 딕셔너리를 하나로 합칠 수도 있다.
# ChainMap과 달리 완전 별개의 dict 생성이므로 원본에 변경이 적용되어야 하면 쓰기엔 불편
a = {'x': 1, 'z': 3}
b = {'y': 2, 'z': 4}
merged = dict(b)
merged.update(a)
print(merged['x'])

