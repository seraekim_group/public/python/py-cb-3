"""
1.4 N 아이템의 최대 혹은 최소 값 찾기
문제: 컬렉션 내부에서 가장 크거나 작은 N개의 아이템을 찾아야 한다.
해결: heapq 모듈
"""

import heapq

nums = [1, 8, 2, 23, 7, -4, 18, 23, 42, 37, 2]
print(heapq.nlargest(3, nums))
print(heapq.nsmallest(3, nums))

portfolio = [
    {'name': 'IBM', 'shares': 100, 'price': 91.1},
    {'name': 'AAPL', 'shares': 50, 'price': 543.22},
    {'name': 'FB', 'shares': 200, 'price': 21.09},
    {'name': 'HPQ', 'shares': 35, 'price': 31.75},
    {'name': 'YHOO', 'shares': 45, 'price': 16.35},
    {'name': 'ACME', 'shares': 75, 'price': 115.65},
]


def l():
    return lambda s: s['price']


cheap = heapq.nsmallest(3, portfolio, key=l())
expensive = heapq.nlargest(3, portfolio, key=l())
print(cheap)
print(expensive)

# 성능
heap = list(nums)
print(heap)
heapq.heapify(heap)
print(heap)

heapq.heappop(heap)  # -4
heapq.heappop(heap)  # 1
heapq.heappop(heap)  # 2

# 최대값 최소값 각각 1개씩 찾는 경우면 max, min이 더 나음.
# N의 크기가 컬렉션 크기와 비슷해지면, sorted(itmes)[:N] / sorted(itmes)[-N:] 사용
