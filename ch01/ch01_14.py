"""
1.14 기본 비교 기능 없이 객체 정렬
문제: 동일한 클래스 객체를 정렬해야 하는데, 이 클래스는 기본적인 비교 연산을 제공하지 않는다.
해결: sorted key lambda 넘겨주거나 attrgetter 와 활용
"""


from operator import attrgetter
class User:
    def __init__(self, user_id):
        self.user_id = user_id

    def __repr__(self):
        return 'User({})'.format(self.user_id)


users = [User(23), User(3), User(99)]
print(sorted(users, key=lambda u: u.user_id))

# attrgetter 를 쓰는게 성능 상 조금 더 좋다
print(sorted(users, key=attrgetter('user_id')))
print(min(users, key=attrgetter('user_id')))
