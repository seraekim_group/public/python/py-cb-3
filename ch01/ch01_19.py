"""
1.19 데이터를 변환하면서 줄이기
문제: 감소 함수(sum, min, max...)를 실행해야 하는데, 먼저 데이터를 변환하거나 필터링해야 한다.
해결: 생성자 표현식
"""

nums = [1, 2, 3, 4, 5]
s = sum(x * x for x in nums)


# .py 파일 체크
import os
files = os.listdir('./')
if any(name.endswith('.py') for name in files):
    print('There be python!')
else:
    print('Sorry, no python.')

# 튜플을 csv로 출력
s = ('ACME', 50, 123.45)
print(','.join(str(x) for x in s))

# 자료 구조의 필드를 줄인다.
portfolio = [
    {'name': 'GOOC', 'shares': 50},
    {'name': 'YHOO', 'shares': 75},
    {'name': 'AOL', 'shares': 10},
    {'name': 'SCOX', 'shares': 65}
]

min_shares = min(s['shares'] for s in portfolio)  # 생성자 표현식
min_shares = min(portfolio, key=lambda s: s['shares'])  # 대안
print(min_shares)

# 생성자를 사용 시 데이터를 순환 가능하게 변형하므로 메모리 측면에서 유리
