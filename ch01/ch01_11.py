"""
1.11 슬라이스 이름 붙이기
문제: 프로그램 코드에 slice를 지시하는 하드코딩이 너무 많아 이해하기 어려운 상황이다.
해결: 이름 붙이기, slice로부터 따로 변수를 만든다
"""

record = '....................100         ........513.25  ..........'
cost = int(record[20:32]) * float(record[40:48])
print(cost)

SHARES = slice(20, 32)
PRICE = slice(40, 48)
cost = int(record[SHARES]) * float(record[PRICE])
print(cost)

items = [0, 1, 2, 3, 4, 5, 6]
a = slice(2, 4)
print(items[a])
items[a] = [10, 11]
print(items)
del items[a]
print(items)

a = slice(5, 20, 2)
print(a.step)

# indices 메소드를 통해 시퀀스에 슬라이스 매핑
s = 'HelloWorld'
print(a.indices(len(s)))
for i in range(*a.indices(len(s))):
    print(s[i])



