"""
1.17 딕셔너리의 부분 추출
문제: 딕셔너리의 특정 부분으로부터 다른 딕셔너리를 만들고 싶다.
해결: dictionary comprehension
"""

prices = {
    'ACME': 45.23,
    'AAPL': 612.78,
    'IBM': 205.55,
    'HPQ': 37.20,
    'FB': 10.75
}

# 가격이 200 이상
p1 = {key: value for key, value in prices.items() if value > 200}
# 기술 관련 주식으로 딕셔너리 구성
tech_names = {'AAPL', 'IBM', 'HPQ', 'MSFT'}
p2 = {key: value for key, value in prices.items() if key in tech_names}
print(p1)
print(p2)

# 튜플 시퀀스 만들고 dict 로 전달
p1 = dict((key, value) for key, value in prices.items() if value > 200)

# dict 튜플 보단 dictionary comprehension이 이 예제에서만 해도 두 배 더 빠르다

# p2를 만드는 방법으로 다음과 같이도 가능하지만 1.6배 느려진다
p2 = {key: prices[key] for key in prices.keys() & tech_names}

# 실행시간과 프로파일링은 11.13절 참고
