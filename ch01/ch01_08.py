"""
1.8 딕셔너리 계산
문제: 딕셔너리 데이터에 여러 계산을 수행하고 싶다(최소값, 최대값, 정렬 등)
해결: zip 또는 lambda key 제공
"""

prices = {
    'ACME': 45.23,
    'AAPL': 612.78,
    'IBM': 205.55,
    'HPQ': 37.20,
    'FB': 10.75
}

min_price = min(zip(prices.values(), prices.keys()))
print(min_price)
max_price = max(zip(prices.values(), prices.keys()))
print(max_price)
prices_sorted = sorted(zip(prices.values(), prices.keys()))
print(prices_sorted)

print(min(prices, key=lambda k: prices[k]))
print(max(prices, key=lambda k: prices[k]))
