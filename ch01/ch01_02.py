"""
1.2 임의 순환체의 요소 나누기
문제: 순환체를 언패킹하는데 요소가 N개 이상 포함되어 "값이 너무 많습니다"라는 예외가 발생한다.
해결: 별 표현식
"""


def drop_first_last(grades):
    first, *middle, last = grades
    return sum(middle)/len(middle)


print(drop_first_last((1, 2, 3, 4, '5')))

# 번호가 하나 이상이든 아예 없든 상관없이 들어간다.
record = ('Srkim', 'srk@example.com', '82-10-1111-2222', '02-2-888-9999')
name, email, *phone_numbers = record
print(phone_numbers)

# 마지막과 나머지를 비교하고 싶은 경우 별표 변수를 앞에 쓴다.
*trail, curr = [1, 2, 3, 4, 5, 6]
trail_avg = sum(trail) / len(trail)
print(trail_avg, curr)

# 태깅
records = [('foo', 1, 2), ('bar', 'hello'), ('foo', 3, 4), ]


def do_foo(x, y):
    print('foo', x, y)


def do_bar(s):
    print('bar', s)


for tag, *args in records:
    if tag == 'foo':
        do_foo(*args)
    elif tag == 'bar':
        do_bar(*args)

# 문자열 프로세싱
line = 'nobody:*:-2:-2:Unprivileged User:/var/empty:/var/empty:/usr/bin/false'

uname, *fields, homedir, sh = line.split(':')
print(fields)

# 특정 값 버리기
record = ('ACME', 50, 123.45, (12, 18, 2012))
name, *_, (*_, year) = record
print(year)

# 머리 꼬리 분리
items = [1, 10, 7, 4, 5, 9]
head, *tail = items


# 재귀 알고리즘 작성
def my_sum(items):
    head, *tail = items
    return head + my_sum(tail) if tail else head


print(my_sum(items))
