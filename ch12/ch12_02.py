"""
12.2 스레드가 시작했는지 판단하기
해결: threading 의 Event 객체 사용
"""

from threading import Thread, Event, Condition, Semaphore
import time


def countdonw(n, started_evt):
    print('countdown starting')
    started_evt.set()
    while n > 0:
        print('T-minus', n)
        n -= 1
        time.sleep(5)


# 시작을 호출하는 데 사용할 이벤트 객체를 생성
started_evt = Event()

# 스레드를 실행하고 시작 이벤트에 전달
# print('Launching countdown')
# t = Thread(target=countdonw, args=(10, started_evt))
# t.start()

# 스레드가 시작하기를 기다림
# started_evt.wait()
# print('countdown is running')

# Launching countdown 다음에 countdown starting 출력되는데
# 이는 메인스레드가 시작 메시지를 출력하는 함수를 기다리기 때문이다.

# 일회성이 아닌 지속적으로 이벤트를 발생시킨다면 Condition...
class PeriodicTimer:
    def __init__(self, interval):
        self._interval = interval
        self._flag = 0
        self._cv = Condition()

    def startI(self):
        t = Thread(target=self.run)
        t.daemon = True
        t.start()

    # 타이머를 실행하고 구간마다 기다리는 스레드에게 알림
    def run(self):
        while True:
            time.sleep(self._interval)
            with self._cv:
                self._flag ^= 1
                self._cv.notify_all()

    def wait_for_tick(self):
        with self._cv:
            last_flag = self._flag
            while last_flag == self._flag:
                self._cv.wait()


# 타이머 사용 예제
ptimer = PeriodicTimer(1)
ptimer.startI()


# 타이머에 싱크하는 두 스레드
def countdown(nticks):
    while nticks > 0:
        ptimer.wait_for_tick()
        print('T-minus', nticks)
        nticks -= 1


def countup(last):
    n = 0
    while n < last:
        ptimer.wait_for_tick()
        print('Counting', n)
        n += 1


Thread(target=countdown, args=(10,)).start()
Thread(target=countup, args=(5,)).start()

# Event 객체의 핵심 기능은 기다리고 있는 모든 스레드를 깨운다는 점.
# 기다리는 스레드 중 하나만 깨우는 프로그램을 만든다면 Semaphore 나 Condition

# 워커 스레드
def worker(n, sema):
    # 호출을 기다림
    sema.acquire()
    # 메시지 출력
    print('Working', n)

# 스레드 생성
sema = Semaphore(0)
nworkers = 10

# 스레드 풀이 시작되지만, 모두 세마포어를 획득하려고 진행이 막혀서 아무일도 안 일어난다.
for n in range(nworkers):
    t = Thread(target=worker, args=(n, sema,))
    t.start()

# 세마포어가 해제될 때마다 하나의 워커만 깨어나서 실행된다.
sema.release()
sema.release()

# 직접 스레드간 동기화를 진행하면 힘들다. queue, actor를 사용하는게 편하다.
