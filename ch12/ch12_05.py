"""
12.5 락킹으로 데드락 피하기
문제: 스레드가 하나 이상의 락을 취득해야 하고 데드락을 피해야 함.
해결: contextmanager
"""

import threading
from contextlib import contextmanager

# threadlocal state
_local = threading.local()


@contextmanager
def acquire(*locks):
    # 객체 식별자로 락 정렬
    lock = sorted(locks, key=lambda x: id(x))

    # 이미 취득한 락의 락 순서가 깨지지 않도록 주의
    acquired = getattr(_local, 'acquired', [])
    if acquired and max(id(lock) for lock in acquired) >= id(locks[0]):
        raise RuntimeError('Lock Order Violation')

    # 모든 락 취득
    acquired.extend(locks)
    _local.acquired = acquired
    try:
        for lock in locks:
            lock.acquire()
        yield
    finally:
        # 취득한 반대 순서로 락 해제
        for lock in reversed(locks):
            lock.release()
        del acquired[-len(locks):]


x_lock = threading.Lock()
y_lock = threading.Lock()


def thread_1():
    while True:
        # 미리 취득한 락을 확인하고, 기존 락이 새롭게 취득하려는 객체 ID 보다 작은 값을 갖도록 강제
        with acquire(x_lock, y_lock):
            print('Thread-1')


def thread_2():
    while True:
        with acquire(y_lock, x_lock):
            print('Thread-2')


t1 = threading.Thread(target=thread_1)
t1.daemon = True
t1.start()

t2 = threading.Thread(target=thread_2)
t2.daemon = True
t2.start()

# 각 함수에서 락 취득 순서가 서로 다르게 진행되지만 아무런 데드락 없이 동작

