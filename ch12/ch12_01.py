"""
12.1 스레드 시작과 정지
"""

import time


def countdown(n):
    while n > 0:
        print('T-minus', n)
        n -= 1
        time.sleep(5)


# 스레드 생성과 실행
from threading import Thread
t = Thread(target=countdown, args=(10,))
t.start()

if t.is_alive():
    print('running')
else:
    print('Completed')


# 오래 실행되거나 백그라운드에서 영원히 실행하는 경우 데몬으로..
t2 = Thread(target=countdown, args=(10,), daemon=True)

# 데몬 스레드는 join 못하지만 main스레드가 종료될 때 자동으로 사라진다는 특징이 있음.

# 종료지시할거면 직접 구현해야 한다.
class CountdownTask:
    def __init__(self):
        self._running = True

    def terminate(self):
        self._running = False

    def run(self, n):
        while self._running and n > 0:
            print('T-minus', n)
            n -= 1
            time.sleep(5)


c = CountdownTask()
t = Thread(target=c.run, args=(10,))
t.start()

c.terminate()
t.join()  # 실제 종료까지 기다림 (필요한 경우)

