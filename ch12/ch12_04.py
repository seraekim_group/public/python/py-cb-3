"""
12.4 임계 영역 락
문제: race condition 피하고 싶다.
해결: threading Lock
"""

import threading


class SharedCounter:
    def __init__(self, initial_value = 0):
        self._value = initial_value
        self._value_lock = threading.Lock()

    def incr(self, delta=1):
        # mutual exclusion, only one thread can access
        with self._value_lock:
            self._value += delta

    def decr(self,delta=1):
        with self._value_lock:
            self._value -= delta


# 병행 실행 수를 제한 시
from threading import Semaphore
import urllib.request

_fetch_url_sema = Semaphore(5)

def fetch_url(url):
    with _fetch_url_sema:
        return urllib.request.urlopen(url)

